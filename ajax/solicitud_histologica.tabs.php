<script type="text/javascript">

// ANTICUERPOS histologico Y FECHA histologico

$(function actualizar_anticuerpos_histologico(){
	 $.ajax({
			type	: 	"POST",
			url		: 	"ajax/actualizar_anticuerpos_histologico.ajax.php",
			beforeSend: function(){
				$('.cargando').css('display','block');
			},
			success: function(datos){
				$('.cargando').css('display','none');
				$("#div_anticuerpos_histologica").html(datos);
			}
		});
});

// TRAER LOS LOGIN, NUMEROEXAMEN, ACTUALIZAR LOS SUB INDICES	
$(function(){
	
	var rut_login = $("#id_rut_login").val();
	var id_nombre_login = $("#id_nombre_login").val();
	var numero_examen = $("#numero_examen_sol_i").val();

	$("#tm_sol_hl").val(rut_login);
	$("#patologo_sol_hl").val(id_nombre_login);
	$("#numero_examen_sol_hl").val(numero_examen);
	
	var id_examen = $("#id_examen").val();
	
	//alert(id_examen);
	$.ajax({
				type	: "POST",
				url		: "ajax/actualizar_subindice_solicitud_histologico.ajax.php",
				data	: "id_examen="+id_examen+
						  "&random="+Math.random(),
				dataType: "html",
				beforeSend : function(){
					$("#div_subindice_histologico").html('Cargando ...');
				},
				success	: function(datos){
					//alert(datos);
					$("#div_subindice_histologico").html(datos);
				}
		});
});

$(function actualizar_fecha_solicitud_histologico(){
	
	var id_examen = $("#id_examen").val();
	
	$.ajax({
				type	: "POST",
				url		: "ajax/actualizar_fecha_solicitud_histologicas.ajax.php",
				data	: "id_examen="+id_examen+
						  "&random="+Math.random(),
				dataType: "html",
				beforeSend : function (){
					
					$("#div_fecha_solicitud_histologica").html('Cargando ...');
				},
				success	: function(datos){
					$("#div_fecha_solicitud_histologica").html(datos);
				}
		});
});


function graba_histologica(){
	
	var fecha_sol_hl           		= $("#fecha_sol_hl").val();
	var patologo_sol_hl  	  		= $("#patologo_sol_hl").val();
	var laminas_sol_hl   	  		= $("#laminas_sol_hl").val();
	var numero_examen_sol_hl   		= $("#numero_examen_sol_hl").val();
	var select_sub_indice_sol_hl	= $("#select_sub_indice_sol_hl").val();
	var id_examen 					= $("#id_examen").val();
	var id_rut_login				= $("#id_rut_login").val();
	
	$("#text_seleccion_hl").val('');
	var grupo_ingresar_hl = new Array();
		
    $("input[name='grupo_ingresar_hl[]']:checked").each(function() {
        grupo_ingresar_hl.push($(this).val());
    });
    
    $("#text_seleccion_hl").val(grupo_ingresar_hl);
    var text_seleccion = $("#text_seleccion_hl").val();
	
	$error = false;
	
	if(select_sub_indice_sol_hl==""){
		alert('Debe seleccionar un sub indice'); return;
	}
	
	if(text_seleccion==""){
		alert('Debe seleccionar algun anticuerpo'); return;
	}
	
		$.ajax({
				type		: "POST",
				url			: "ajax/grabar_solicitud_histologica.ajax.php",
				data		: "text_seleccion="+text_seleccion+
							  "&fecha_sol_hl="+fecha_sol_hl+
							  "&patologo_sol_hl="+patologo_sol_hl+
							  "&laminas_sol_hl="+laminas_sol_hl+
							  "&numero_examen_sol_hl="+numero_examen_sol_hl+
							  "&select_sub_indice_sol_hl="+select_sub_indice_sol_hl+
							  "&id_examen="+id_examen+
							  "&random="+Math.random(),
				beforeSend: function(){
					
				},
				success: function(datos){
					datos = $.trim(datos);
					if(datos == 1){
						alert ("Los datos se han grabado correctamente");
						$("input[class=grupo_ingresar_hl]").removeAttr('checked');					 
						$("#div_solicitud_inmuhistoquimica").dialog("close");	
					}
					if(datos == 2){
						alert ("Ha ocurrido un error, comuniquese con Informatica");
					}		
				}
			});			  	
}


function buscar_anticuerpos_de_solicitud_histologicas(id_registro){
	var id_registro = $("#fecha_sol_hl").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/buscar_solicitudes_anticuerpos_histologicas.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			$("input[class=grupo_ingresar_hl]").attr("checked", false);
			
			var datos = datos.split(",");			
 			$.each(datos, function(index, value) {
	  			$("input[id=anticuerpo_hl_"+value+"]").attr("checked", true);		  			
			});
		}
	});	
	
	$.ajax({
		type		: "POST",
		url			: "ajax/buscar_solicitudes_subindices_histologicas.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			datos = $.trim(datos);
			$('#select_sub_indice_sol_hl').val(datos);	
		}
	});	
}  

function eliminar_histologica(){
	var id_registro = $("#fecha_sol_hl").val();
	
	
	$.ajax({
		type		: "POST",
		url			: "ajax/eliminar_solicitudes_anticuerpos_histologicas.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			datos = $.trim(datos);
			if(datos == 1){				
				alert ("Los datos se han eliminado correctamente");
				solicitud();
				$('#select_sub_indice_sol_hl').val('');
			}
			
			if(datos == 2){
				alert ("Ha ocurrido un error, comuniquese con Informatica");
			}	
		}
	});	
}

function resolver_histologica(){
	var id_registro = $("#fecha_sol_hl").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/resolver_histologica.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			datos = $.trim(datos);
			
			if(datos == 1){
				alert ("Se ha resuelto correctamente");
			}else{
				alert ("Ha ocurrido un error, comuniquese con Informatica");
			}
		}
	});	
}

function salir_dialogo_anticuerpos(){
	var dialog = $("#div_solicitud_inmuhistoquimica").dialog(opt_solicitud_examenes_anticuerpos);
	dialog.dialog('close');
}
/*-------------------------------------------------*/
/*-------------------------------------------------*/
/*-------------------------------------------------*/



</script>

<table border="0" class="tabla_listado">
		<tr>
			<td colspan="100%" align="center">
				<h4>Laboratorio Anatomía Patológica</h4>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				Fecha Solicitud<br>
				<div id="div_fecha_solicitud_histologica">
					
				</div>
			</td>
			<td>
				Patólogo Solicitante<br>
				<input  id="patologo_sol_hl" type="text" disabled>
			</td>
			<td colspan="3">
				Láminas H-E<br>
				<select id="laminas_sol_hl">
					<option value='S'>Si se adjuntan</option>
					<option value='N'>No se adjuntan</option>
				</select>
			</td>
			<td>
				Nº Exámen<br>
				<input  id="numero_examen_sol_hl" value="" type="text" size="9" disabled>
			</td>
			<td>
				Subindice<br>
				<div id="div_subindice_histologico">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="100%" align="center">
				<div id="div_anticuerpos_histologica" style="text-align:center; width: 100%" >
					<!--<img src="imagenes/cargando.gif" style="width: 50px; height: 50px; display: none;" class="cargando" >-->
					<div style="display: none; font-size: 20" class="cargando" >
						<BR/><BR/>
						Cargando ...
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<table >
				<td colspan="100%">
					<tr>
						<table border="0" class="tabla_listado">
							<tr>
								<td style="display: none;">
									<?
									$fecha = time();
									$fecha =  date("d/m/Y H:i",time());
									?>
									Fecha de Solicitud:
								</td>
								<td style="display: none;">
									<input  id="fecha_sol_hl" value="<?= $fecha ?>" type="text" readonly >
								</td>
								<td>
									 <button id="id_salir" style="width:80px; margin-left:10px;" onclick="graba_histologica()">
								        <img src="imagenes/Symbol-Check.png" style="width:30px; height:30px;" /><br />
								            Grabar
								    </button>
								</td><br /><td>
									 <button id="id_salir" style="width:80px; margin-left:10px;" onclick="eliminar_histologica()">
								        <img src="imagenes/Symbol-Delete.png" style="width:30px; height:30px;" /><br />
								            Eliminar 
								    </button>
								</td>
								</td><br /><td>
									 <button style="width:80px; margin-left:10px;" onclick="salir_dialogo_anticuerpos()">
								        <img src="imagenes/salir.png" style="width:30px; height:30px;" /><br />
								            Salir 
								    </button>
								</td>
								<!--
								<td>
									<button id="id_salir" style="width:80px; margin-left:10px;" onclick="resolver_histoquimica()" >
								        <img src="imagenes/firma.png" style="width:30px; height:30px;" /><br />
								            Resolver
								    </button>
								</td>
								-->
							</tr>
							<tr>	
								<td style="display: none;">
									Responsable:
								</td>
								<td style="display: none;">
									<input  id="tm_sol_hl" type="text" readonly >
								</td>
							</tr>
						</table>
					</tr>
				</td>
			</table>
		</tr>
	</table>