<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
</head>
<link href="../css/estilos_impresion.css" rel="stylesheet" type="text/css" media="all"/>
<body>
<?php
if(session_id()==''){
	session_start();
}

include("../config/conectar_bd.php");
include("../config/funciones_f.php");

$xrut_login = $_SESSION['rut_login'];
$xperfil=$_SESSION['perfil'];

foreach($_GET as $nombre_campo => $valor){
   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
   eval($asignacion);
} 

$fecha = time();
$fecha =  date("d/m/Y H:i",time()) ;

setlocale(LC_ALL,"es_ES@euro","es_ES","esp");

$fecha=fecha_texto($fecha);
$cSql="SELECT 
           *,
		   pacientes.nombres as nombre_paciente,
		   pacientes.primer_apellido as p_apellido_paciente, 
		   pacientes.segundo_apellido as s_apellido_paciente,
		   medicos.nombres as nombre_med,
		   medicos.apellido_paterno as apellido_paterno_med,
		   medicos.apellido_materno as apellido_materno_med 
		 FROM examenes
		 INNER JOIN	pacientes ON examenes.rut_paciente = pacientes.rut
		 INNER JOIN medicos ON examenes.rut_profesional_solicita = medicos.rut	  
         INNER JOIN	unidades ON examenes.id_unidad = unidades.id_unidad		  
		 WHERE id_examen = $id_registro;";

$query=pg_Exec($conexion, $cSql);
$row=pg_fetch_array($query);

//$edad=42;
$id_examen=$row['id_examen'];
$examen_macroscopico=$row['examen_macroscopico'];
?>

<table width="700" border="0" class="table_principal">
<tr>
<td colspan="6" style="font-weight:bold;font-size:14px">MINISTERIO DE SALUD 
</td>
<td colspan="3" rowspan="4"><img src="../imagenes/logo.png" width="180" height="160" /></td><td width="0"></td><td width="0"></td><td width="0"></td><td width="0"></td><td width="0"></td>
</tr>

<tr>
<td colspan="6" style="font-weight:bold;font-size:14px">SERVICIO DE SALUD METROPOLITANO NORTE
</td>
<td></td><td></td><td></td><td></td><td></td><td width="0"></td>
</tr>
<tr>
<td colspan="6" style="font-weight:bold;font-size:14px">INSTITUTO  NACIONAL DEL CANCER
</td>
<td></td><td></td><td></td><td></td><td></td><td></td>
</tr>
<tr>
<td colspan="6" style="font-weight:bold;font-size:14px">ANATOMIA PATOLOGICA
</td>
<td></td><td></td><td></td><td></td><td></td><td></td>
</tr>


<tr>
<td width="694" ><br><br></td>
</tr>


<tr>
<td colspan="9" align="center" class="valor_black" style="font-weight:bold;font-size:14px">SOLICITUD DE EXAMEN DE ANATOMIA PATOLOGICA N° &nbsp;&nbsp;<?php echo $row['modalidad'].' '.$row['numero_examen'];?></td>

</tr>
<tr>
  <td colspan="9" align="left"><table width="750" border="0">
    <tr>
      <td colspan="6" class="td_estilo_redondo"><table width="750" border="0" style="padding-top:10px;">
        <tr>
            <td width="177" style="text-align:left;font-weight:bold">Estado</td>
            <td width="10">:</td>
            <td width="225" style="text-align:left">
<?php 
$cSql="SELECT * FROM examen_estado INNER JOIN estados ON examen_estado.id_estado = estados.id_estado WHERE id_examen= $id_registro";
//echo $cSql; 
$queryEs=pg_Exec($conexion, $cSql);
$rowEs=pg_fetch_array($queryEs);
echo '<img src="../imagenes/'.$rowEs['icono'].'" style="width:16px; height:16px;" />&nbsp;&nbsp; '.$rowEs['descripcion_estado'];
?>
            </td>
            <td width="69" style="text-align:left"><span style="text-align:left;font-weight:bold">N° de Ficha</span></td>
            <td width="9" style="text-align:left">:</td>
            <td width="55" style="text-align:left"><?php echo $row['numero_ficha'];?></td>
            <td width="61" style="text-align:left;font-weight:bold">Condición</td>
            <td width="11">:</td>
            <td width="95" style="text-align:left">
              <?php 
                    $condicion='Hospitalizado';
					if ($row['A']){
                      $condicion='Ambulatorio';						
					}
					echo $condicion;
					?>
           </td>
          </tr>
      </table>
        <table width="750" border="0">
          <tr>
            <td width="178" style="text-align:left;font-weight:bold">Rut Paciente</td>
            <td width="10">:</td>
            <td width="263" style="text-align:left"><?php 
              $dv=dv($row[1]);
			  echo $row[1].'-'.$dv;?></td>
            <td width="31" style="text-align:left;font-weight:bold">Sexo</td>
            <td width="9">:</td>
            <td width="54" style="text-align:left"><?php echo $row['sexo']?></td>
            <td width="62" style="text-align:left;font-weight:bold">Edad</td>
            <td width="12">:</td>
            <td width="93" style="text-align:left"><?php echo $row['edad'];?></td>
          </tr>
        </table>
        <table width="750" border="0" style="padding-bottom:10px;">
          <tr>
            <td width="177" style="text-align:left;font-weight:bold">Nombre Paciente</td>
            <td width="9">:</td>
            <td width="372" style="text-align:left"><?php echo $row['nombre_paciente'].' '.$row['p_apellido_paciente'].' '.$row['s_apellido_paciente'];?></td>
            <td width="61" style="text-align:left;font-weight:bold">Previsión</td>
            <td width="13">:</td>
            <td width="92" style="text-align:left"><?php echo $row['prevision']?></td>
          </tr>
        </table></td>
      </tr>
    <tr>
      <td colspan="6" class="td_estilo_redondo" style="padding-top:10px"><table width="750" border="0">
        <tr>
          <td width="181" style="text-align:left;font-weight:bold">Fecha de Solicitud</td>
          <td width="10">:</td>
          <td width="154" style="text-align:left"><?php echo cambiarFormatoFechaHora($row['fecha_aud']);?></td>
          <td width="111" style="text-align:left;font-weight:bold">Medico Solicitante</td>
          <td width="9">:</td>
          <td width="266" style="text-align:left"><?php echo strtoupper($row['nombre_med'].' '.$row['apellido_paterno_med'].' '.$row['apellido_materno_med'])?></td>
          </tr>
      </table>
        <table width="750" border="0" style="padding-bottom:10px;">
          <tr>
            <td width="179" style="text-align:left;font-weight:bold">Solicitado por</td>
            <td width="10">:</td>
            <td width="547" style="text-align:left"><?php echo pg_traercampo("SELECT nombre FROM funcionarios WHERE rut=".$row['rut_digita_aud'],$conexion);?> | <?php pg_traercampo("SELECT nombre FROM funcionarios WHERE rut=".$row['rut_digita_aud'],$conexion);
				echo $row['rut_digita_aud'].'-'.dv($row['rut_digita_aud']).'&nbsp;&nbsp;&nbsp;&nbsp;';?></td>
            </tr>
        </table><table width="750" border="0"><tr>
        </tr>
      </table></td>
      </tr>
    <tr>
      <td colspan="6" class="td_estilo_redondo"><table width="750" border="0" style="padding-top:10px;">
        <tr>
          <td width="180" style="text-align:left;font-weight:bold">Intervencion Quirurgica Actual</td>
          <td width="11">:</td>
          <td style="text-align:left"><?php /*?>$cSqlp="SELECT * FROM fonasa WHERE codigo = '".substr($row['intervencion_actual'],0,7)."';";
$queryS = pg_Exec($conexion, $cSqlp);
while ($rowf=pg_fetch_array($queryS)) {echo $rowf['descripcion'].'<br>';}
*/
$cSqlqx="SELECT intervencion_actual FROM examenes WHERE id_examen = $id_registro;";
$queryqx=pg_Exec($conexion, $cSqlqx);
$rowqx=pg_fetch_array($queryqx);
echo $rowqx['intervencion_actual'];
?></td>
        </tr>
      </table>
        <table width="750" border="0">
          <tr>
            <td width="180" style="text-align:left;font-weight:bold">Sospecha Diagnostica</td>
            <td width="11">:</td>
            <td style="text-align:left"><?php /*?>$cSqlp="SELECT * FROM cieo_diagnostico WHERE codigo = '".$row['diagnosticos_clinicos']."';";
$queryS = pg_Exec($conexion, $cSqlp);
$rowf=pg_fetch_array($queryS);
echo $rowf['descripcion'];*/
$cSqlp="SELECT diagnosticos_clinicos FROM examenes WHERE id_examen = $id_registro;";
$queryS = pg_Exec($conexion, $cSqlp);
$rowf=pg_fetch_array($queryS);
echo $rowf['diagnosticos_clinicos'];
?></td>
          </tr>
        </table>
        <table width="750" border="0">
          <tr>
            <td width="180" style="text-align:left;font-weight:bold">Estudios Especiales</td>
            <td width="11">:</td>
            <td style="text-align:left"><?php echo $row['estudios_especiales'];?></td>
          </tr>
        </table>
        <table width="750" border="0" style="padding-bottom:10px;">
          <tr>
            <td width="180" style="text-align:left;font-weight:bold">Antecedentes</td>
            <td width="11">:</td>
            <td style="text-align:left"><?php echo $row['antecedentes'];?></td>
          </tr>
        </table></td>
      </tr>
    <tr>
      
      </tr>
  </table></td>
</tr>
<tr>
  
</tr>
<tr>
<td colspan="9"><table width="755"  height="5" border="1" class="td_estilo_redondo">
  <tr>
      <td align="center" class="subtitulo" style="text-align:left">MUESTRAS</td>
    </tr>
</table>
</td>
</tr>


<td colspan="9">
<?php 
$numero_examen=$row['numero_examen'];
include("actualizar_muestras_solicitud.ajax.php");
echo $sM;
?>
</td>
</tr>


</table>
<script type="text/javascript">
	window.print();
</script>





