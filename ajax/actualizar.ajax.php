
<?php
header('content-type: text/html; charset=utf-8');

if(session_id()==''){
	session_start();
}

include("../config/conectar_bd.php");
include("../config/funciones_f.php");


if (isset($_SESSION['rut_login'])==false ){
 echo "<br>No se ha iniciado sesión<br><br>";
 echo "<td><a href='intranet.incancer.cl'>Haga click aquí para iniciar una nueva sesión</a></td>" ;	
 return false;
}	


//Variables de SESSION
$xperfil	=	$_SESSION['perfil'];
$xrut_login	=	$_SESSION['rut_login'];
$xunidad	=	$_SESSION['unidad'];
$xinformes  =   $_SESSION['informes'];
$ahora = time();

foreach($_POST as $nombre_campo => $valor){
   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
   eval($asignacion);
   
   //echo "$asignacion <BR/>";
 } 
//echo $xperfil;

//Busca si es un medico el que esta logueado
$queryMe=pg_Exec($conexion, "SELECT rut FROM medicos WHERE rut=".$xrut_login."");	
$rowMe=pg_fetch_array($queryMe);
$xrut_medico=$rowMe['rut'];




//Variables para reporte:
$totalr=0;	
$nom_paciente='';

if($perfil_buscar == 11){
	$csql="SELECT  examenes.id_examen,
									       examenes.rut_paciente, 
									       (pacientes.nombres || ' ' || pacientes.primer_apellido || ' ' || pacientes.segundo_apellido) AS nombre_paciente, 
									       (medicos.nombres || ' ' ||  medicos.apellido_paterno || ' ' || medicos.apellido_materno) as vob,
									       informes.informe_digitalizado,
									       (informes.fecha::timestamp::date) as fecha_informe,
									       informes.fecha_entrega_informe,
									       informes.fecha_informe_archivado,
									       informes.id_registro,
									       examenes.numero_examen
									FROM informes as informes
									INNER JOIN examenes as examenes 
									ON informes.id_examen = examenes.id_examen
									INNER JOIN pacientes as pacientes
									ON pacientes.rut = examenes.rut_paciente 
									INNER JOIN medicos as medicos
									ON medicos.rut = informes.vob
									WHERE informe_digitalizado is not null 
									AND fecha_informe_archivado is null 
									ORDER BY
		   fecha DESC";
//echo $csql;									
	$result = pg_query($conexion, $csql) or die("ERROR: ".pg_last_error());	
	$contador  = 0;
	while ($row = pg_fetch_array($result)) {
		$informe_digitalizado[$contador] 		= $row['informe_digitalizado'];
		$fecha_entrega_informe[$contador] 		= $row['fecha_entrega_informe'];
		$fecha_informe_archivado[$contador]  	= $row['fecha_informe_archivado'];
		$fecha_informe[$contador]  				= $row['fecha_informe'];
		$rut_paciente[$contador]  				= $row['rut_paciente'];
		$nombre_paciente[$contador]  			= $row['nombre_paciente'];
		$vob[$contador] 						= $row['vob'];
		$id_registro[$contador] 				= $row['id_registro'];
		$numero_examen[$contador] 				= $row['numero_examen'];
		
		$contador ++;
	} 
	
 ?>
 <table class="tabla_listado"  style="width:100%">
  <tr>
     <td class="cabecera">
        <? echo $_SESSION['nombre_login'].' '.date("d/m/Y H:i",$ahora); ?>
     </td>
  </tr>
   <tr>
     <td>
      <table width="100%">
          <tr>
          	<td align="center" class="campoclic " style="width:auto;">Estado</td>
          	<td align="center" class="campoclic " style="width:auto;">N° Biopsia</td>    
          	<td align="center" class="campoclic " style="width:auto;">N° Informe</td>                             
            <td align="center" class="campoclic " style="width:auto;">Informes</td>                      			           			
            <td align="center" class="campoclic " style="width:auto;">Fecha</td>   
            <td align="center" class="campoclic" style="width:auto;">Rut Paciente</td>
            <td align="center" class="campoclic" style="width:auto;">Nombre del Paciente</td>
            <td align="center" class="campoclic" style="width:auto;">Medico Responsable Informe</td>
            <td align="center" class="campoclic" style="width:auto;">Acción</td>                                    			                            
          </tr>
          <? for($i=0;$i<$contador;$i++){?>
          	          				
          	<tr class="mano">
          		<td algin="center" class="valor_listado"> 
          			<? if($fecha_entrega_informe[$i] == ""){
          				echo "Para retirar";
          			   }else{
						if($fecha_informe_archivado[$i] == ""){
	          				echo "Para archivar";
          			    }
					   }
          			?>
          			
          		</td>
          		<td algin="center" class="valor_listado"> <?= $numero_examen[$i];?> </td>
          		<td algin="center" class="valor_listado"> <?= $id_registro[$i];?> </td>
          		<td class="valor_listado">
				    <img title="Informe disponible para consulta" ondblclick="abre_pdf('<?
					$year=substr(get_fecha($fecha_informe[$i]),6,4);
					echo $year.'/'.$id_registro[$i];
					?>.pdf')" style="width:20px; height:30px;" src="imagenes/informe.png"></img>
				</td>
          		<td algin="center" class="valor_listado"> <?= date('d-m-Y', strtotime($fecha_informe[$i]))?> </td>
          		<td algin="center" class="valor_listado"> <?= $rut_paciente[$i]?> </td>
          		<td algin="center" class="valor_listado"> <?= $nombre_paciente[$i]?> </td>
          		<td algin="center" class="valor_listado"> <?= $vob[$i]?> </td>
          		<td algin="center" class="valor_listado"> 
          			<? 
          				if($fecha_entrega_informe[$i] == ""){
          			?>	
          					<img src="imagenes/recibir.png" style="width:30px; height:30px;" onclick="retirar_informe(<?=$id_registro[$i];?>)" id="img_retirar"> 

          			<?
          			   }else{
						if($fecha_informe_archivado[$i] == ""){
	          		?>		
	          				<img src="imagenes/archivar_informe.jpg" style="width:30px; height:30px;" onclick="archivar_informe(<?=$id_registro[$i];?>)" id="img_archivar">
	          		<?
          			    }
					   }

          			?>
          		</td>
          	</tr>		
          <?		
          	} 
          ?>
       </table>

 <? 
 
      
}else{
	
if ($perfil_buscar != $xperfil){
 $xperfil=$perfil_buscar;	
}


$condicion=' true ';
$condicion.=" AND estado is null ";

if ($id_numero_ficha_bus!=''){
 $queryPl=pg_Exec($conexion, "SELECT rut FROM pacientes WHERE numero_ficha='".$id_numero_ficha_bus."'");	
 
 //echo "SELECT rut FROM pacientes WHERE numero_ficha='".$id_numero_ficha_bus."'";
 
 $rowPl=pg_fetch_array($queryPl);
 $xrut=$rowPl['rut'];
}


if ($xrut!=''){ 
  $axrut=explode('-',$xrut);
  $condicion.=" AND rut_paciente=".$axrut[0]."";
}

if($id_lugar_buscar != $xunidad && $id_lugar_buscar != ''){
	if ($xperfil!='2' && $id_lugar_buscar !=''){
	  $condicion.=" AND examenes.id_unidad=$id_lugar_buscar";
	}
}
//echo 'x'.$id_lugar_buscar.'x<br>';
//echo 'x'.$id_fecha_desde_buscar.'x<br>';


//$id_fecha_hasta_buscar =  date('Y-m-d', strtotime($id_fecha_hasta_buscar));

if($id_fecha_desde_buscar != "" && $id_fecha_hasta_buscar == "" ){
	$id_fecha_desde_buscar = str_replace("/", "-", "$id_fecha_desde_buscar");
	$id_fecha_desde_buscar_strtotime =  date("Y-m-d", strtotime("$id_fecha_desde_buscar"));
	
	$condicion.= "AND (fecha_aud::timestamp::date)  = '$id_fecha_desde_buscar_strtotime' ";
	
}

if($id_fecha_hasta_buscar != "" && $id_fecha_desde_buscar != ""){ 
	$id_fecha_desde_buscar = str_replace("/", "-", "$id_fecha_desde_buscar");
	$id_fecha_desde_buscar_strtotime =  date("Y-m-d", strtotime("$id_fecha_desde_buscar"));
	
	$id_fecha_hasta_buscar = str_replace("/", "-", "$id_fecha_hasta_buscar");
	$id_fecha_hasta_buscar_strtotime =  date("Y-m-d", strtotime("$id_fecha_hasta_buscar"));
	
	$condicion.= "AND (fecha_aud::timestamp::date) >= '$id_fecha_desde_buscar_strtotime' AND (fecha_aud::timestamp::date)<='$id_fecha_hasta_buscar_strtotime'";
	
}
 
if($id_numero_examen_bus != ""){
	 $condicion.= "AND examenes.numero_examen = '$id_numero_examen_bus'";
	
}


if($id_mis_buscar != ""){
 
 if ($xperfil == '1' || $xrut_medico != '' ){
	 $condicion.= "AND examenes.rut_digita_aud = '$id_mis_buscar'";
 }
	
}

//if ($xunidad != ''  && $id_lugar_buscar == '' ){
if($id_lugar_buscar == $xunidad  && $xunidad != ''){
	
	
  $axunidad=explode(',',$xunidad);	
	
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0].") ";
  
 // echo $condi_unidad;
  
  if (isset($axunidad[1]) && $axunidad[1] != ''){
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0]." OR examenes.id_unidad  = ".$axunidad[1]." ) ";	  
  }
  if (isset($axunidad[2]) && $axunidad[2] != ''){
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0]." OR examenes.id_unidad  = ".$axunidad[1]." OR examenes.id_unidad  = ".$axunidad[2]." ) ";	  
  }
  if (isset($axunidad[3]) && $axunidad[3] != ''){
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0]." OR examenes.id_unidad  = ".$axunidad[1]." OR examenes.id_unidad  = ".$axunidad[2]." OR examenes.id_unidad  = ".$axunidad[3]." ) ";	  
  }

  if (isset($axunidad[4]) && $axunidad[4] != ''){
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0]." OR examenes.id_unidad  = ".$axunidad[1]." OR examenes.id_unidad  = ".$axunidad[2]." OR examenes.id_unidad  = ".$axunidad[3]."  OR examenes.id_unidad  = ".$axunidad[4]."  ) ";	  
  }

  if (isset($axunidad[5]) && $axunidad[5] != ''){
  $condi_unidad=" AND (examenes.id_unidad  = ".$axunidad[0]." OR examenes.id_unidad  = ".$axunidad[1]." OR examenes.id_unidad  = ".$axunidad[2]." OR examenes.id_unidad  = ".$axunidad[3]."  OR examenes.id_unidad  = ".$axunidad[4]." OR examenes.id_unidad  = ".$axunidad[5]."  ) ";	  
  }


  
  $condicion.=$condi_unidad;  
}

 if ($xperfil=='4' ){               //Macroscopia
  $condicion.=" AND (examen_estado.id_estado  = 3 
                      OR 
					 examen_estado.id_estado =  4
					  OR 
					 examen_estado.id_estado =  5 
					  OR 
					 examen_estado.id_estado =  8 			 )";
 }

 if ($xperfil=='5' ){               //Procesamiento Histologico - Inclusion
  $condicion.=" AND (examen_estado.id_estado  = 5 
                OR 
				  examen_estado.id_estado  = 6
                OR 
				 examen_estado.id_estado =  8 			)";
 }

 if ($xperfil=='6' ){               //Microscopia - Inclusion
  $condicion.=" AND ( examen_estado.id_estado  = 8 )";
 }

if ($xperfil =='9'){
  $condicion.=" AND examenes.rut_profesional_solicita=".$xrut_login;
}

if ($xperfil =='10'){
  $condicion.=" AND examen_estado.id_estado  = 14";
}

if ($xperfil =='2'){
  $condicion.=" AND (examen_estado.id_estado  = 1 or examen_estado.id_estado  = 12)";
}

if ($xperfil =='12'){  //Custodia
  $condicion.=" AND examen_estado.id_estado  = 1";
}


//echo $xperfil.$condicion;

$cSql="SELECT  
		 *
	   FROM 
		examenes
	   INNER JOIN
         unidades 
       ON
		 examenes.id_unidad = unidades.id_unidad
	 
	   INNER JOIN
		   examen_estado
		 ON 
		   examenes.id_examen = examen_estado.id_examen   
	   
	   INNER JOIN
		   estados
		 ON 
		   examen_estado.id_estado = estados.id_estado   
		 
		WHERE  ".$condicion."
		
		ORDER BY
		   fecha_aud DESC;";


//echo $cSql;		   
$query=pg_Exec($conexion, $cSql);	

//echo $cSql; 

if (!$query) {
  echo "Error: ".pg_last_error($conexion);
}

 // Sistema de Trazabilidad de Biopsias '. date("d/m/Y H:i",$ahora) .' - '.$_SESSION['nombre_login'].'

/*$exunidad = explode(",",$xunidad);
$cantidadstringunidad = count($exunidad);

$ss = "<select id='id_unidad_cambiar' onchange='cambiar_unidad()'>
       	<option value=''>--Seleccione--</option>";
foreach($exunidad as $i =>$key) {
  //  echo $i.' '.$key .'</br>';
	$result = pg_query($conexion,"SELECT descripcion_unidad FROM unidades WHERE id_unidad = $key") or die ("ERROR : ".pg_last_error());
	
	while($row = pg_fetch_array($result)){
			$descripcionunidad = $row['descripcion_unidad'];
			$ss .= "<option value='$key'>$descripcionunidad</option>";				
	}	
}

$ss .= "</select>";
*/
$s='<table class="tabla_listado"  style="width:100%">
  <tr>
     <td class="cabecera">
        '.$_SESSION['nombre_login'].' '.date("d/m/Y H:i",$ahora).'
     </td>
  </tr>
   <tr>
     <td>
      <table width="100%">
          <tr>
            <td align="center" class="campoclic " style="width:5px;"></td>                                
            <td align="center" class="campoclic " style="width:5px;">Estado</td>                     
            <td align="center" class="campoclic " style="width:5px;">Informes</td>                      			           			
            <td align="center" class="campoclic " style="width:5px;">Fecha</td>                      
            <td align="center" class="campoclic " style="width:5px;">Hora</td>
            <td align="center" class="campoclic " style="width:5px;">Transcurrido</td>			
            <td align="center" class="campoclic" style="width:7px;">Rut</td>
            <td align="center" class="campoclic" style="width:auto;">Nombre del Paciente</td>
            <td align="center" class="campoclic" style="width:auto;">Unidad</td>                        
            <td align="center" class="campoclic" style="width:auto;">Clase</td>                        
            <td align="center" class="campoclic" style="width:auto;">Presentación</td>                        						
            <td align="center" class="campoclic" style="width:auto;">Nº Muestras</td>                        			
            <td align="center" class="campoclic" style="width:auto;">Nº Biopsia</td>                        
            <td align="center" class="campoclic" style="width:auto;">Plazo</td>                        			                            
            <td align="center" class="campoclic" style="width:auto;">Acción</td>    			
          </tr>
         ';
		 


while ($row=pg_fetch_array($query)){

$id_examen = $row['id_examen'];		


 
//Busca el estado del examen		 

$fecha_aud = $row['fecha_aud'];


 //Busca los datos del paciente
 $queryPacientes=pg_Exec($conexion, "SELECT 
								   * 
								 FROM 
								   pacientes
								  WHERE
									rut=".$row['rut_paciente'].";");	
									
 $rowPacientes = pg_fetch_array($queryPacientes);
 ////////////////////////////////////////////////////////////
 
 
 
 //Busca las muestras asociadas al examen
 $condicion_muestras='true';
 
 if ($xperfil=='2' ){
  $condicion_muestras.=" AND rut_traslado is null";
 }
 if ($xperfil=='3' ){
  $condicion_muestras.=" AND rut_traslado is not null and rut_recepcion is null";

 }
 
 $sql="SELECT 
		   * 
		 FROM 
		   muestras
		  WHERE
			id_examen='".$row['id_examen']."' ;";
 $queryMuestras=pg_Exec($conexion, $sql);	
 $clased='';
 $claser=''; 
 $clasec=''; 
 $contador_diferida=1;
 $contador_rapida=1;
 $contador_citologia=1;  
 while ($rowMuestras = pg_fetch_array($queryMuestras)){
     
	 if ($rowMuestras['tipo']=='D'){
		  $contador='';
		  $clased='Diferida ['.$contador_diferida.']<br>';
		  $contador_diferida++;
	 }

	 if ($rowMuestras['tipo']=='R'){
		  $contador='';
		  $claser='Rápida ['.$contador_rapida.']<br>';
		  $contador_rapida++;
	 }
	 
	 if ($rowMuestras['tipo']=='C'){
		  $contador='';
		  $clasec='Citología ['.$contador_citologia.']<br>';
		  $contador_citologia++;
	 }

								
 }


 $sql="SELECT 
		   * 
		 FROM 
		   muestras
		  WHERE
			id_examen='".$row['id_examen']."' AND ".$condicion_muestras.";";
			
 //echo "<br>".$sql;
 
 $queryMuestras=pg_Exec($conexion, $sql);	
 $nro_muestras=0;									

 $presentacion_frascos='';
 $presentacion_tubos='';
 $presentacion_laminas='';  
 $presentacion_tacos='';   
 $contador_frascos=1; 
 $contador_tubos=1; 
 $contador_laminas=1; 
 $contador_tacos=1;  
 
   
 while ($rowMuestras = pg_fetch_array($queryMuestras)){
     $fecha_recepcion=$rowMuestras['fecha_recepcion'];

	 if ($rowMuestras['presentacion']=='F'){
	  $nro_muestras=$nro_muestras + 1;			 
	  $presentacion_frascos='Frascos ['.$contador_frascos.']<br/>';
	  $contador_frascos++; 
	 }
	 else
	 {
	  $nro_muestras=$nro_muestras + $rowMuestras['cantidad'];	
	 }

	 if ($rowMuestras['presentacion']=='T'){
	  $presentacion_tubos='&nbsp;&nbsp;&nbsp;Tubos ['.$rowMuestras['cantidad'].']<br/>';
	  $contador_tubos++; 
	 }
	 
	 if ($rowMuestras['presentacion']=='L'){
//	  $presentacion_laminas='Láminas ['.$rowMuestras['cantidad'].']<br/>';

	  $presentacion_laminas='Láminas ['.$contador_laminas.']<br/>';	  
	  $contador_laminas++; 	  
	 }
	 if ($rowMuestras['presentacion']=='A'){
	  $presentacion_tacos='Tacos ['.$rowMuestras['cantidad'].']<br/>';
	  $contador_tacos++; 
	 }
								
 }


 ////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////
// Busca cuales registros estan finalizados en Macroscopia para visualizarlos en Procesamiento Histologico
////////////////////////////////////////////////////////////////////////////////////////////
// echo $xperfil;
 if ( $xperfil=='5' ){
 
     $mostrar=false;
	 $condicion_macroscopia=' true ';
	 $sql="SELECT 
			   * 
			 FROM 
			   macroscopia
			  WHERE
				id_examen = ".$row['id_examen']." AND estado_cassette = 9;";
				
	 //echo "<br>".$sql;
	 
	 $queryMacro=pg_Exec($conexion, $sql);	
	 while ($rowMacro = pg_fetch_array($queryMacro)){
	   if($xperfil=='5' && $rowMacro['rut_corte']==''){  //Procesamiento Automatico
		   $mostrar = true;
	  }								
	 }
  
 }
 else
 {
   $mostrar=true;	 
 }
 

//////////////////////////////////////////////////////////////

$informe_img='';
$imagen_audio='';
$editar=true;
//echo "<br>".$xperfil . $nro_muestras ;
if (($xperfil =='2' || $xperfil =='3') && $nro_muestras =='0') {	
  $mostrar=false;
}

if ($xperfil =='9' ) {	
  $editar=false;
}

$informe_archivado=false;

$arch=$_SESSION['upload_folder']."imagenes_digitalizadas/adjunto_".$row['id_examen'].".pdf";

if (file_exists($arch) ) {
        $informe_img.='<img src="imagenes/clip.png" style="width:20px; height:20px;" ondblclick="abre_pdf_solicitud(\'adjunto_'.$row['id_examen'].'.pdf\')" title="Biopsia digitalizada en Endoscopía" />&nbsp;';	 		 
}

 $sql="SELECT 
		   id_registro,examen_microscopico,nombre_archivo,vob,audio,resultado_critico,informe_digitalizado,rut_archiva_informe,fecha_archiva_informe 
		 FROM 
		   informes
		  WHERE
			id_examen = ".$row['id_examen']."
			ORDER BY
			fecha; ";
 $queryMicro=pg_Exec($conexion, $sql);	

 while ($rowMicro = pg_fetch_array($queryMicro)){


 if ($row['otro_centro'] == '' && $rowMicro['examen_microscopico'] ==''){
        $informe_img.='<img src="imagenes/informando.png" style="width:20px; height:30px;" ondblclick="reconocimiento_voz('.$rowMicro['id_registro'].',\''.$row["numero_examen"].'\','.$row['id_examen'].',\''.$nom_paciente.'\')" title="Informe en transcripción de grabación de voz" />&nbsp;';	 	
		$mostrar = true	 ;
		if ($xperfil == '3'){
		   $editar=false;
		}

	}

 if ($rowMicro['examen_microscopico'] !='' && $rowMicro['vob'] =='0'){
        $informe_img.='<img src="imagenes/firma.png" style="width:25px; height:25px;" ondblclick="reconocimiento_voz('.$rowMicro['id_registro'].',\''.$row["numero_examen"].'\','.$row['id_examen'].',\''.$nom_paciente.'\')"  title="Informe enviado para firma y validación"/>&nbsp;';	 	
		$mostrar = true	 ;
		if ($xperfil == '3'){
		$editar=false;
		}

	 }

 if ($rowMicro['resultado_critico'] !='S' && $rowMicro['examen_microscopico'] !='' && $rowMicro['vob'] != '0'){
        $informe_img.='<img src="imagenes/informe.png" style="width:20px; height:30px;" ondblclick="abre_pdf_menu(\''.$rowMicro['informe_digitalizado'].$rowMicro['nombre_archivo'].'\')" title="Informe disponible para consulta" />&nbsp;';	 		 
	 }


 if ($rowMicro['resultado_critico'] == 'S' && $rowMicro['vob'] != '0'){
        $informe_img.='<img src="imagenes/informe_critico.png" style="width:20px; height:30px;" ondblclick="abre_pdf_menu(\''.$rowMicro['informe_digitalizado'].$rowMicro['nombre_archivo'].'\')" title="Resultado critico disponible para consulta" />&nbsp;';	 		 

	 }


//Para las solicitudes enviadas a otro centro de salud:
 if ($row['otro_centro'] != '' && $rowMicro['vob'] != '0' && $rowMicro['resultado_critico'] == 'S'){
        $informe_img.='<img src="imagenes/informe.png" style="width:20px; height:30px;" ondblclick="abre_pdf_menu(\''.$rowMicro['informe_digitalizado'].$rowMicro['nombre_archivo'].'\')" title="Resultado critico disponible para consulta" />&nbsp;';	 		 

	 }


/*
	if($rowMicro['audio'] == "s"){
	  	$imagen_audio .= '<img src="imagenes/audio.png" style="width:20px; height:20px;" /> &nbsp';
	}
*/

 }
 
//echo $mostrar;

if (($xperfil =='2') && $nro_muestras =='0') {	
  $mostrar=false;
}

//Si el prefil es de Macroscopia solo muestra los registros en los que el medico grabo la macro
//echo "<br>".$xperfil.' '.$xrut_medico.' '.$row['rut_macroscopia'];
if ($xperfil=='4' && $row['id_estado'] !='3'){

	if ($xrut_login	!= $row['rut_macroscopia'] ){
	  // $mostrar=false;

	}
 
}
	
 
 $xfecha 				= $row['fecha_aud'];
 $estado_img			='<img src="imagenes/'.$row['icono'].'" style="width:20px; height:20px;" />';
 $descripcion_estado 	= $row['descripcion_estado'];
  if ($rowMuestras['vob_recepcion'] !='1'){
	  $descripcion_estado.='  ';
  }
  if ($row['otro_centro'] !=''){
	 $result_oc=pg_Exec($conexion, "SELECT descripcion FROM establecimientos WHERE cod_establecimiento='".$row['otro_centro']."'");	 
	 $rowoc=pg_fetch_array($result_oc);


	  
	  if ($rowMicro['vob'] != '0' &&  $informe_img!=''){
	  $descripcion_estado='Recibido de  '.$rowoc['descripcion'];	  
	  }
	  else
	  {
	  $descripcion_estado.='  '.$rowoc['descripcion'];		  
		  }
  }
 
	 $xahora=$ahora;

	 $result_i=pg_Exec($conexion, "SELECT fecha_vob FROM informes WHERE id_examen=".$row['id_examen']);	 
	 $rowi=pg_fetch_array($result_i);

	 if ($rowi['fecha_vob']!=''){
	 	 $xahora=strtotime($rowi['fecha_vob']);
	 }
    
     $dias_transcurrido='';
	 $dias_plazo='';
	 if ($row['id_plazo']!=''){
		 $result_pl=pg_Exec($conexion, "SELECT dias FROM plazo WHERE id_plazo=".$row['id_plazo']);	 
		 $rowPlazo=pg_fetch_array($result_pl);
		 $dias_plazo=$rowPlazo['dias'];
		 $maximo_dias=$rowPlazo['dias'];
		 $transcurrido=$xahora - strtotime($fecha_recepcion) ;
		 $dias_transcurrido = dias_plazo($transcurrido); 
	 }


	 $plazo='<img src="imagenes/yellow.png" style="width:20px; height:20px;"/>';   //Aun esta en el plazo
	 if ($dias_plazo!='' && $dias_plazo < $dias_transcurrido){
     	 $plazo='<img src="imagenes/red.png" style="width:20px; height:20px;"/>';		 //Entrgo o no ha entregado en el plazo
	 }
	 
	 
	 if ($rowi['fecha_vob'] !='' && $dias_transcurrido <= $dias_plazo ){
     	 $plazo='<img src="imagenes/green.png" style="width:20px; height:20px;"/>';		 //Entrego en el plazo
	 }
	 
	

//$plazo.=''.get_fecha($transcurrido); 
  
 
	 $fecha = get_fecha($xfecha);
	 $hora  = get_hora($xfecha); 
	 $transcurrido=$xahora - strtotime($xfecha) ;
	
	 $transcurrido = conversor_tiempo($transcurrido); 
	
	 if ($mostrar == true) {
     $totalr++;
	 $numero_examen=$row["numero_examen"];
	 $modalidad=$row['modalidad'];
	 if ($modalidad==''){
		 $modalidad='&nbsp;';
		 }
	 $num=$row['numero_examen'];
	 if ($row['numero_examen']!=''){
	  $num=$row['numero_examen'].' '.$modalidad	 ;
	 }
	 $nom_paciente=$rowPacientes['nombres'].' '.$rowPacientes['primer_apellido'].' '.$rowPacientes['segundo_apellido'];
	 $s.='<tr class="mano"  >'; 
	 $s.='<td class="valor_listado" width="20">'.$estado_img.'</td>';			   
	 $s.='<td class="valor_listado" width="200">'.$descripcion_estado.'</td>';			  
	 $s.='<td class="valor_listado">&nbsp;'.$informe_img.' '.$imagen_audio.'&nbsp</td>';			   
	 $s.='<td class="valor_listado">'.$fecha.'</td>';			  
	 $s.='<td class="valor_listado">'.$hora.'</td>';			 
	 $s.='<td class="valor_listado" align="center">'.$transcurrido.'</td>';			 	 
	 $s.='<td class="valor_listado">'.str_replace(',', '.', number_format($rowPacientes['rut']).'-'.$rowPacientes['dv']).'</td>';			 
	 $s.='<td class="valor_listado">'.htmlentities(utf8_decode($rowPacientes['nombres'])).' '.htmlentities(utf8_decode($rowPacientes['primer_apellido'])).' '.htmlentities(utf8_decode($rowPacientes['segundo_apellido'])).'</td>'; 				
	 $s.='<td class="valor_listado">'.htmlentities(utf8_decode($row['descripcion_unidad'])).'</td>';	 
	 $s.='<td class="valor_listado">&nbsp;'.$clased.$claser.$clasec.'</td>';	 		     
	 $s.='<td class="valor_listado">&nbsp;'.$presentacion_frascos.$presentacion_tubos.$presentacion_laminas.$presentacion_tacos.'</td>';
	 $s.='<td class="valor_listado">'.$nro_muestras.'</td>';	 
	 $s.='<td class="valor_listado" align="right">'.$num.'&nbsp;</td>';	  



	 //$plazo.=$transcurrido;	 
	 if ($fecha_recepcion==''){	 
      $s.='<td  class="valor_listado" >&nbsp;</td>';
	 }
	 else
	 {
	 if ($dias_plazo==''){
       $s.='<td  class="valor_listado" >&nbsp;</td>';	 
		 }
      else
	  {
	  $s.='<td  class="valor_listado" >'.$dias_transcurrido.'/'.$dias_plazo.'&nbsp;'.$plazo.'</td>';	 
	  }
	 }
	 
	 if ($dias_plazo==''){
//	  $s.='<td class="valor_listado"><img src="imagenes/ver.png" style="width:30px; height:20px;" onclick="trayectoria('.$row["id_examen"].');" /></td>';		 	  
	 }
	  if ($editar == true){	
	     $s.='<td class="valor_listado" ondblclick="selecciona_registro(\''.$numero_examen.'\','.$row['id_examen'].',\''.$nom_paciente.'\')"><img src="imagenes/Edit.png" style="width:20px; height:20px;"   /></td>';		 	 
	  }
	  else
	  {
	     $s.='<td class="valor_listado">&nbsp;</td>';		 	 
		  
		  }
	 //$s.='<td class="valor_listado"><img src="imagenes/time.png" style="width:30px; height:30px;" onclick="trayectoria('.$row["id_examen"].');" /></td>';		 	 
	 $s.='<td class="valor_listado"><img src="imagenes/ver.png" style="width:40px; height:25px;" onclick="trayectoria('.$row["id_examen"].');" /></td>';		 
	 
	 
	 $s.='</tr>';
 }
}
$s.=' </table>
</td>
</tr>
<tr>
<td colspan="3">
*Total de Registros '.$totalr.' </td>
</tr>
</table>';

echo $s;

}
pg_free_result($query); 

?>
                    



