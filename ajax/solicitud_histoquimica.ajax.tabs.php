<script type="text/javascript">

// ANTICUERPOS HISTOQUIMICO Y FECHA HISTOQUIMICO
$(function (){
	 $.ajax({
			type	: 	"POST",
			url		: 	"ajax/actualizar_anticuerpos_histoquimico.ajax.php",
			beforeSend: function(){
				$('.cargando').css('display','block');
			},
			success: function(datos){
				$('.cargando').css('display','none');
				$("#div_anticuerpos_histoquimica").html(datos);
			}
		});
		
});		

// TRAER LOS LOGIN, NUMEROEXAMEN, ACTUALIZAR LOS SUB INDICES
$(function (){
	var rut_login = $("#id_rut_login").val();
	var id_nombre_login = $("#id_nombre_login").val();
	var numero_examen = $("#numero_examen_sol_i").val();

	$("#tm_sol_hq").val(rut_login);
	$("#patologo_sol_hq").val(id_nombre_login);
	$("#numero_examen_sol_hq").val(numero_examen);
	
	var id_examen = $("#id_examen").val();
	$.ajax({
				type	: "POST",
				url		: "ajax/actualizar_subindice_solicitud_histoquimicas.ajax.php",
				data	: "id_examen="+id_examen+
						  "&random="+Math.random(),
				dataType: "html",
				beforeSend : function(){
					$("#div_subindice_histoquimica").html('Cargando ...');
				},
				success	: function(datos){
					$("#div_subindice_histoquimica").html(datos);
				}
		});
});

$(function actualizar_anticuerpos_inmunohistoquimica(){
	$.ajax({
		type	: 	"POST",
		url		: 	"ajax/actualizar_anticuerpos_histoquimico.ajax.php",
		beforeSend: function(){
			$('.cargando').css('display','block');
		},
		success: function(datos){
			$('.cargando').css('display','none');
			$("#div_anticuerpos_histoquimica").html(datos);
		}
	});
});

$(function actualizar_fecha_solicitud_histoquimico(){
	
	var id_examen = $("#id_examen").val();
	
	$.ajax({
				type	: "POST",
				url		: "ajax/actualizar_fecha_solicitud_histoquimicas.ajax.php",
				data	: "id_examen="+id_examen+
						  "&random="+Math.random(),
				dataType: "html",
				beforeSend : function (){
					
					$("#div_fecha_solicitud_histoquimica").html('Cargando ...');
				},
				success	: function(datos){
					$("#div_fecha_solicitud_histoquimica").html(datos);
				}
		});
});

function graba_histoquimica(){
	
	var fecha_sol_hq           		= $("#fecha_sol_hq").val();
	var patologo_sol_hq  	  		= $("#patologo_sol_hq").val();
	var laminas_sol_hq   	  		= $("#laminas_sol_hq").val();
	var numero_examen_sol_hq   		= $("#numero_examen_sol_hq").val();
	var select_sub_indice_sol_hq	= $("#select_sub_indice_sol_hq").val();
	var id_examen 					= $("#id_examen").val();
	var id_rut_login				= $("#id_rut_login").val();
	
	//alert(id_rut_login);
	
	$("#text_seleccion").val();
	var grupo_ingresar_hq = new Array();
		
    $("input[name='grupo_ingresar_hq[]']:checked").each(function() {
        grupo_ingresar_hq.push($(this).val());
    });
    
    $("#text_seleccion").val(grupo_ingresar_hq);
    var text_seleccion = $("#text_seleccion").val();
	
	$error = false;
	
	if(select_sub_indice_sol_hq==""){
		alert('Debe seleccionar un sub indice'); return;
	}
	
	if(text_seleccion==""){
		alert('Debe seleccionar algun anticuerpo'); return;
	}
	
		$.ajax({
				type		: "POST",
				url			: "ajax/grabar_solicitud_histoquimica.ajax.php",
				data		: "text_seleccion="+text_seleccion+
							  "&fecha_sol_hq="+fecha_sol_hq+
							  "&patologo_sol_hq="+patologo_sol_hq+
							  "&laminas_sol_hq="+laminas_sol_hq+
							  "&numero_examen_sol_hq="+numero_examen_sol_hq+
							  "&select_sub_indice_sol_hq="+select_sub_indice_sol_hq+
							  "&id_examen="+id_examen+
							  "&random="+Math.random(),
				beforeSend: function(){
					
				},
				success: function(datos){
					datos = $.trim(datos);
					if(datos == 1){
						alert ("Los datos se han grabado correctamente");
						$("input[class=grupo_ingresar_hq]").removeAttr('checked');					 
						$("#div_solicitud_inmuhistoquimica").dialog("close");	
					}
					if(datos == 2){
						alert ("Ha ocurrido un error, comuniquese con Informatica");
					}		
				}
			});			  	
}


function buscar_anticuerpos_de_solicitud_histoquimica(id_registro){
	var id_registro = $("#fecha_sol_hq").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/buscar_solicitudes_anticuerpos_histoquimica.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			$("input[class=grupo_ingresar_hq]").attr("checked", false);
			
			var datos = datos.split(",");			
 			$.each(datos, function(index, value) {
	  			$("input[id=anticuerpo_hq_"+value+"]").attr("checked", true);		  			
			});
		}
	});	
	
	$.ajax({
		type		: "POST",
		url			: "ajax/buscar_solicitudes_subindices_histoquimica.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			datos = $.trim(datos);
			$('#select_sub_indice_sol_hq').val(datos);	
		}
	});	
}  


function eliminar_histoquimica(){
	var id_registro = $("#fecha_sol_hq").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/eliminar_solicitudes_anticuerpos.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			
			datos = $.trim(datos);
			if(datos == 1){				
				alert ("Los datos se han eliminado correctamente");
				solicitud();
				$('#select_sub_indice_sol_hq').val('');
			}
			
			if(datos == 2){
				alert ("Ha ocurrido un error, comuniquese con Informatica");
			}	
		}
	});	
}


function resolver_histoquimica(){
	var id_registro = $("#fecha_sol_hq").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/resolver_hisotquimica.ajax.php",
		data		: "id_registro="+id_registro+
					  "&random="+Math.random(),			
		success	: function(datos){
			datos = $.trim(datos);
			
			if(datos == 1){
				alert ("Se ha resuelto correctamente");
			}else{
				alert ("Ha ocurrido un error, comuniquese con Informatica");
			}
		}
	});	
}
	
function salir_dialogo_anticuerpos(){
	var dialog = $("#div_solicitud_inmuhistoquimica").dialog(opt_solicitud_examenes_anticuerpos);
	dialog.dialog('close');
}
/* ---------------------------------------------------- */
/* ---------------------------------------------------- */
/* ---------------------------------------------------- */
/* ---------------------------------------------------- */
/* ---------------------------------------------------- */
/* ---------------------------------------------------- */

</script>

<table border="0" class="tabla_listado">
		<tr>
			<td colspan="100%" align="center">
				<h4>Laboratorio Anatomía Patológica</h4>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				Fecha Solicitud<br>
				<div id="div_fecha_solicitud_histoquimica">
					
				</div>
			</td>
			<td>
				Patólogo Solicitante<br>
				<input  id="patologo_sol_hq" type="text" disabled>
			</td>
			<td colspan="3">
				Láminas H-E<br>
				<select id="laminas_sol_hq">
					<option value='S'>Si se adjuntan</option>
					<option value='N'>No se adjuntan</option>
				</select>
			</td>
			<td>
				Nº Exámen<br>
				<input  id="numero_examen_sol_hq" value="" type="text" size="9" disabled>
			</td>
			<td>
				Subindice<br>
				<div id="div_subindice_histoquimica">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="100%" align="center">
				<div id="div_anticuerpos_histoquimica" style="text-align:center; width: 100%" >
					<!--<img src="imagenes/cargando.gif" style="width: 50px; height: 50px; display: none;" class="cargando" >-->
					<div style="display: none; font-size: 20" class="cargando" >
						<BR/><BR/>
						Cargando ...
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<table >
				<td colspan="100%">
					<tr>
						<table border="0" class="tabla_listado">
							<tr>
								<td style="display: none;">
									<?
									$fecha = time();
									$fecha =  date("d/m/Y H:i",time());
									?>
									Fecha de Solicitud:
								</td>
								<td style="display: none;">
									<input  id="fecha_sol_hq" value="<?= $fecha ?>" type="text" readonly >
								</td>
								<td>
									 <button id="id_salir" style="width:80px; margin-left:10px;" onclick="graba_histoquimica()">
								        <img src="imagenes/Symbol-Check.png" style="width:30px; height:30px;" /><br />
								            Grabar
								    </button>
								</td><br /><td>
									 <button id="id_salir" style="width:80px; margin-left:10px;" onclick="eliminar_histoquimica()">
								        <img src="imagenes/Symbol-Delete.png" style="width:30px; height:30px;" /><br />
								            Eliminar 
								    </button>
								</td>
								</td><br /><td>
									 <button style="width:80px; margin-left:10px;" onclick="salir_dialogo_anticuerpos()">
								        <img src="imagenes/salir.png" style="width:30px; height:30px;" /><br />
								            Salir 
								    </button>
								</td>
								<!--
								<td>
									<button id="id_salir" style="width:80px; margin-left:10px;" onclick="resolver_histoquimica()" >
								        <img src="imagenes/firma.png" style="width:30px; height:30px;" /><br />
								            Resolver
								    </button>
								</td>
								-->
							</tr>
							<tr>	
								<td style="display: none;">
									Responsable:
								</td>
								<td style="display: none;"> 
									<input  id="tm_sol_hq" type="text" readonly >
								</td>
							</tr>
						</table>
					</tr>
				</td>
			</table>
		</tr>
	</table>