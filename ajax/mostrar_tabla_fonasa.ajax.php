<? include('../config/conectar_bd.php');

	$result = pg_query($conexion, "SELECT codigo, descripcion FROM fonasa WHERE codigo != '0000000'") or die("ERRROR : ".pg_last_error());

?>
<script type="text/javascript">

$(function(){
	var id_lugar_buscar 	= $("#id_lugar_buscar").val();
	
	$.ajax({
		type		: "POST",
		url			: "ajax/buscar_codigos_fonasa_personalizados.ajax.php",
		data		: "id_lugar_buscar="+id_lugar_buscar+
					  "&random="+Math.random(),			
		success	: function(datos){
			
		$("input[class=codigofonasa]").attr("checked", false);
		
				var datos = datos.split(",");			
				$.each(datos, function(index, value) {
					$("input[id=codigofonasa_"+value+"]").attr("checked", true);		  			
					});
			}	
	});	
});
	
function guardar_seleccion(){
	
var grupo_ingresar = new Array();
	
$("input[name='grupo_ingresar[]']:checked").each(function() {
    grupo_ingresar.push($(this).val());
});

$("#check_seleccionados").val(grupo_ingresar);

var check_seleccionados = $("#check_seleccionados").val();
var id_lugar_buscar 	= $("#id_lugar_buscar").val();
//alert(check_seleccionados);

if(check_seleccionados==""){
	alert('Debe seleccionar algun codigo'); return;
}

if(id_lugar_buscar==""){
	alert('Debe seleccionar alguna unidad'); return;
}

//alert(id_lugar_buscar);

$.ajax({
		type		: "POST",
		url			: "ajax/grabar_codigos_personalizados.ajax.php",
		data		: "check_seleccionados="+check_seleccionados+
					  "&id_lugar_buscar="+id_lugar_buscar+
					  "&random="+Math.random(),
		beforeSend: function(){
		},
		success: function(datos){
			datos = $.trim(datos);
			//alert(datos);
			if(datos == 1){
				alert ("Los datos se han grabado correctamente");
						
			}
			if(datos != 1){
				alert ("Ha ocurrido un error, comuniquese con Informatica");
			}		
		}
	});			  		
}

function salir_personalizar_fonasa(){
	var dialogo_tabla_fonasa = $("#div_tabla_fonasa").dialog(opt_tabla_codigo_fonasa);
	dialogo_tabla_fonasa.dialog('close');
	

}
</script>

	<input type="text" id="check_seleccionados" style="display: none;"/>
	<div style="float: right;">
		<button onclick="guardar_seleccion()"> Guardar Selección </button>
		<button onclick="salir_personalizar_fonasa()">Salir</button>
	</div>
	<div style=" height: 600px; width: 700px;overflow: auto;">
		<table class="tabla_listado" style=" overflow:scroll; "> 
			<tbody>
				<tr>
					<td colspan="9" align="center">
						<h4>Codigo Fonasa</h4>
					</td>
				</tr>
				<tr> 
					<td>
					<? 
						$c = 0;  
						
						while ($row=pg_fetch_array($result)){
							
							$codigo_fonasa[$c] = $row['codigo'];
							$descripcion[$c] = $row['descripcion'];
							
							$j = $c - 1;
							
							if($codigo_fonasa[$c] == $codigo_fonasa[$j]){
								echo "$descripcion[$c]  ";
							}else{
								
								echo "</td></tr><tr><td style='border-bottom:1px solid black;'>";
					?>
								<input class="codigofonasa" id="codigofonasa_<?=$codigo_fonasa[$c]?>" type='checkbox' value='<?=$codigo_fonasa[$c]?>' name='grupo_ingresar[]' >
					<?
								echo "$codigo_fonasa[$c] -- $descripcion[$c]";
							}
							
							$c++;	
						}
					?>
					</td>
				</tr>	
			</tbody>	
		</table>
	</div>
	