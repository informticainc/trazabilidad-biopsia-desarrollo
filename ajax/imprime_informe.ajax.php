<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
</head>


<link href="../css/estilos_impresion.css" rel="stylesheet" type="text/css" media="all"/>

<body>

<?php

if(session_id()==''){
	session_start();
}


include("../config/conectar_bd.php");
include("../config/funciones_f.php");

$xrut_login = $_SESSION['rut_login'];
//$xperfil=$_SESSION['perfil'];

foreach($_GET as $nombre_campo => $valor){
   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
   eval($asignacion);
} 

$fecha = time();
$fecha =  date("d/m/Y H:i",time()) ;

setlocale(LC_ALL,"es_ES@euro","es_ES","esp");

$fecha=fecha_texto($fecha);
$cSql="SELECT 
		  pacientes.nombres||' '||primer_apellido||' '||segundo_apellido as nombre_paciente,
		  pacientes.rut as rut_paciente,
		  medicos.nombres||' '||apellido_paterno||' '||apellido_materno as nombre_medico,
		  fecha_aud, antecedentes, numero_ficha, edad, diagnosticos_clinicos
		 FROM examenes INNER JOIN pacientes ON examenes.rut_paciente = pacientes.rut 
		 INNER JOIN medicos ON examenes.rut_profesional_solicita = medicos.rut WHERE id_examen = $id_registro;";

$query=pg_Exec($conexion, $cSql);
$row=pg_fetch_array($query);

//echo $cSql;

$edad=0;

?>

<table width="500" border="0" class="table_principal">
<tr>
<td colspan="6">MINISTERIO DE SALUD<br/>
                SERVICIO DE SALUD METROPOLITANO NORTE<br/>
                INSTITUTO  NACIONAL DEL CANCER<br/>
                ANATOMIA PATOLOGICA
                
</td>
<td colspan="8" ><img src="../imagenes/logo.png" alt="" width="70" height="80" /></td>
</tr>



<tr>
<td ><br><br></td>
</tr>


<tr>
<td colspan="8" align="center" class="valor_black">INFORME <?php echo strtoupper($clase_informe);?> DE BIOPSIA No. &nbsp;&nbsp;<?php echo $row['modalidad'].$numero_informe_inf;?></td>
</tr>

<tr>
<td class="campo" >
NOMBRE:</td>
<td class="valor" width="249"><?php echo $row['nombre_paciente'];?></td>
<td width="15" class="campo">Edad</td><td class="valor"><?php echo $row['edad'].' ';?></td><td></td><td></td><td width="25"></td><td></td>
</tr>

<tr>
<td class="campo" >
No. DE FICHA:</td>
<td  class="valor" ><?php echo $row['numero_ficha'];?></td>
<td></td><td></td><td></td><td></td><td></td><td></td>
</tr>

<tr>

<td class="campo" >
RUT:</td>
<td  class="valor" ><?php 
              $dv=dv($row['rut_paciente']);
			  echo $row['rut_paciente'].'-'.$dv;?></td>
<td></td><td></td><td></td><td></td><td></td><td></td>              
</tr>

<tr>

<td class="campo" >
MUESTRA:</td>
<td class="valor">
<?php 
$cSql="SELECT 
		 descripcion 
		FROM
		
		muestras
		
		INNER JOIN
		
		cieo_diagnostico
		
		ON
		
		muestras.cod_cieo = cieo_diagnostico.codigo
		
		WHERE

		   id_examen = $id_registro;";

$queryM=pg_Exec($conexion, $cSql);
while($rowM=pg_fetch_array($queryM)){
 $muestra=$rowM['descripcion']."<br>";
}

echo $muestra;

?></td>
<td class="valor">&nbsp;</td>
<td class="valor">&nbsp;</td>
<td class="valor">&nbsp;</td>
<td class="valor">&nbsp;</td>
<td class="valor">&nbsp;</td>
<td class="valor">&nbsp;</td>

</tr>

<tr>

<td class="campo" >
DIAGNOSTICOS CLINICOS:</td>
<td class="valor" ><?php echo $row['diagnosticos_clinicos'];?></td>
<td></td><td></td><td></td><td></td><td></td><td></td>
</tr>



<td class="campo" >
ANTECEDENTE:</td>
<td class="valor"  colspan="5"><?php echo $row['antecedentes'];?></td>
<td></td><td></td>
</tr>

<tr>

<td class="campo" >
FECHA DE RECEPCION:</td>
<td class="valor" ><?php echo cambiarFormatoFechaHora($row['fecha_aud']);?></td>


<td class="campo"  >
SOLICITADO POR EL Dr.(a):</td>
<td class="valor"  colspan="5"><?php echo $row['nombre_medico'];?></td>
</tr>


<tr>
<td><br /><br />
</td>
</tr>

<tr  <?php 
     if ($examen_macroscopico_inf==''){
	   echo ' style="display:none" ';
	 }
	 ?>>
<td colspan="9" style="width:680px;">
  <table width="750"  height="5" border="1">
    <tr>
      <td align="center" class="subtitulo">EXAMEN MACROSCOPICO</td>
    </tr>
  </table>
</td>
</tr>

<?php
$zSql="SELECT examen_macroscopico, examen_microscopico, diagnostico_histologico FROM informes WHERE id_examen='$id_registro'";
$queryM=pg_Exec($conexion, $zSql);
$rowM=pg_fetch_array($queryM);

$ancho_em=90;
$limpia_txt_em = str_replace("<br />", "", nl2br($rowM['examen_macroscopico']));
$cadena_em = $limpia_txt_em;

//Ahora esta es la parte importante del codigo, 
//donde hacemos comparaciones y ajustamos el contenido 
//y lo dividimos en lineas
if (strtoupper(substr(PHP_OS,0,3)=='WIN')) { 
  $eol_em="\r\n"; 
} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) { 
  $eol_em="\r"; 
} else { 
  $eol_em="\n"; 
} 
$cad_em=wordwrap($cadena_em, $ancho_em, $eol_em, 1); 
$lineas_em=substr_count($cad_em,$eol_em)+1;
?>

<tr>
<td colspan="9" align="justify" class="valor_parrafo"><p>
<textarea name="examen_macroscopico" id="examen_macroscopico" style="border:none;overflow:hidden" cols="<?php echo $ancho_em ?>" 
rows="<?php echo $lineas_em ?>"><?php echo $cadena_em ?></textarea>
</p></td>
</tr>


<tr>
<td colspan="9">
  <table width="750"  height="5" border="1">
  <tr>
      <td  align="center" class="subtitulo"><?php 
	                                                       if($clase_informe=='Complementario'){
															   echo "INFORME COMPLEMENTARIO";
															   }
															   else
															   {
																echo 'EXAMEN MICROSCOPICO';   
															   }
														   ?></td>
    </tr>
</table>
</td>
</tr>




<?php
$ancho_ema=90;
$limpia_txt_ema = str_replace("<br />", "", nl2br($rowM['examen_microscopico']));
$cadena_ema = $limpia_txt_ema;
if (strtoupper(substr(PHP_OS,0,3)=='WIN')) { 
  $eol_ema="\r\n"; 
} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) { 
  $eol_ema="\r"; 
} else { 
  $eol_ema="\n"; 
} 
$cad_ema=wordwrap($cadena_ema, $ancho_ema, $eol_ema, 1); 
$lineas_ema=substr_count($cad_ema,$eol_ema)+1;
?>
<tr>
<td colspan="9" class="valor_parrafo" align="justify"><p>
<textarea name="examen_microscopico" id="examen_microscopico" style="border:none;overflow:hidden" cols="<?php echo $ancho_ema ?>" rows="<?php echo $lineas_ema ?>"><?php echo $cadena_ema ?></textarea></p></td>
</tr>



<tr  <?php 
     if ($examen_macroscopico_inf==''){
	   echo ' style="display:none" ';
	 }
	 ?>>
<td colspan="9">
  <table width="750"  height="5" border="1">
    <tr>
      <td align="center" class="subtitulo">DIAGNOSTICO HISTOLOGICO</td>
    </tr>
  </table>
</td>
</tr>


<?php
$ancho_dg=90; 
$limpia_txt_dg = str_replace("<br />", "", nl2br($rowM['diagnostico_histologico']));
$cadena_dg = $limpia_txt_dg;
if (strtoupper(substr(PHP_OS,0,3)=='WIN')) { 
  $eol_dg="\r\n"; 
} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) { 
  $eol_dg="\r"; 
} else { 
  $eol_dg="\n"; 
} 
$cad_dg=wordwrap($cadena_dg, $ancho_dg, $eol_dg, 1); 
$lineas_dg=substr_count($cad_dg,$eol_dg)+1;
?>
<tr>
<td colspan="9" align="justify" class="valor_parrafo"><p><textarea name="diagnostico_histologico" id="diagnostico_histologico" style="border:none;overflow:hidden" cols="<?php echo $ancho_dg ?>" rows="<?php echo $lineas_dg ?>"><?php echo $cadena_dg ?></textarea></p></td>
</tr>

<tr>
<td colspan="1">
<?php
$result = pg_query($conexion, "SELECT * FROM codigos_informes WHERE id_examen=$id_registro") or die ("ERROR : ".pg_last_error());
$s='<table class="tabla_tr_informes"  style="width:100%" border = "0">';
//$s.='<tr style="font-size:8px">';
//$s.='<td width="12">Biopsia</td>';
//$s.='<td width="12">Cie-10</td>';
$s.='<tr>';
$s.='<td class="campo"></td>';
$s.='<td class="campo">CieO</td>';
$s.='<td class="campo">Cie-10</td>';

//$s.='<td width="12">Fonasa</td>';
//$s.='<td>Multiplicador</td>';
$s.='</tr>';

while ($row=pg_fetch_array($result)){
	$tipo='';
	if($row['tipo_biopsia']=='1'){$tipo='Inc';}
	if($row['tipo_biopsia']=='2'){$tipo='Agu';}
	if($row['tipo_biopsia']=='3'){$tipo='Exi';}
	if($row['tipo_biopsia']=='4'){$tipo='Amp';}		
	//$s.='<tr  style="font-size:8px">';
	$s.='<tr>';
	$s.='<td class="valor">'.$tipo.'</td>';
	$s.='<td class="valor">'.$row['cie_o'].'</td>';
	$s.='<td class="valor">'.$row['cie_10'].'</td>';	
	//$s.='<td>'.$row['codigo_fonasa'].'</td>';
	//$s.='<td>'.$row['multiplicador'].'</td>';
	$s.='</tr>';
}
$s.='</table>';
echo $s;
?>
</td>
</tr>



<tr>
<td colspan="9" align="center" style=""><br /><br /><br />
<?php 
$responsable_informe=pg_traercampo("SELECT nombres||' '||apellido_paterno||' '||apellido_materno FROM medicos WHERE rut=".$responsable_informe,$conexion);
echo 'Dr. (a) '.$responsable_informe;?>
</td>
</tr>
<tr>
<td colspan="9" align="center">
<?php echo $fecha;?>
</td>
</tr>

</table>

<script type="text/javascript">
	window.print();
</script>




