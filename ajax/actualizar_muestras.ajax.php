<script type="text/javascript">
	
	function buscar_fonasa(){
	
		var id_lugar_sol = $("#id_lugar_sol").val();
		
		//alert(id_lugar_sol);
		
		$('#id_fonasa_autocompletar').autocomplete(
		{	
		   // source: "ajax/busqueda_autocompletar_fonasa.ajax.php?id_lugar_sol="+id_lugar_sol,
		   	    source: "ajax/busqueda_autocompletar_intervencion.ajax.php",
		    minLength: 2,
			select: function( event, ui ) {
		
					var descripcion = ui.item.label;
					
					//alert(descripcion);
					
					var descripcion_split = descripcion.split("-");
					$('#id_fonasa_autocompletar').val(descripcion_split[0]+" - "+descripcion_split[1]);
					var codigo = descripcion_split[0];
					$("#id_fonasa_sol").val(codigo);
					
	                return false;
	        },
			change:     function(event,ui) {
			}
		});
	
	}
	
</script>

<?
if(session_id()==''){

	session_start();
}

require_once("../config/conectar_bd.php");
require_once("../config/funciones_f.php");

$xrut_login = $_SESSION['rut_login'];
$xperfil=$_SESSION['perfil'];


foreach($_POST as $nombre_campo => $valor){
   $asignacion = "\$" . $nombre_campo . "='" . $valor . "';";
   eval($asignacion);
   
 // echo $asignacion."<BR/>";
} 

$columna_tipo='';

if ($id_lugar_sol=='7' || $_SESSION['unidad'] == '7'){
	$columna_tipo=' style="display:none" ';
}

$condicion='true';
$s1='';
if(isset($id_examen) && $id_examen !=''){
	$condicion.=" AND id_examen = '".$id_examen."' ";
	}
else
{
    $condicion.=" AND (id_examen is null and rut_digita_aud = $xrut_login) ";
}	
/*
	
	 INNER JOIN 
	 	cieo_organos 
	 ON 
	 	muestras.cod_cieo = cieo_organos.codigo	
*/
$cSql="SELECT  
		 * 
	 FROM 
	muestras
	Left JOIN 
	 	cieo_organos 
	 ON 
	 	muestras.cod_cieo = cieo_organos.codigo	
	 WHERE
	   ".$condicion." 
	 ";
//echo $cSql;	   
$query=pg_Exec($conexion, $cSql);	
if ($query==''){
 echo "Error nuevo: ".pg_last_error($conexion);	
 return;
}


//echo $cSql;

$contador = 0;
$s1='<table class="tabla_listado"  style="width:100%">	
		  <tr>
			  <td class="campoclic" width="195">Organo</td>
			  <td class="campoclic" width="30">Presentación</td>
			  <td class="campoclic" width="50">Cantidad</td>
			  <td class="campoclic" width="80">Lateralidad</td>
			  <td class="campoclic" width="70" id="id_columna_tipo" '.$columna_tipo.'>Tipo</td>
			  <td class="campoclic" width="50">Muestra</td>
			  <td class="campoclic" width="30"></td>
		 </tr>';
				 
while ($row=pg_fetch_array($query)){
	
	//$des_iact[$contador]=$row['codigo_fonasa'];
	$lateralidad[$contador] = $row['lateralidad'];
	$codigo[$contador] = $row['cod_cieo'];
	$organo[$contador] = $row['organo'];
	$descripcion[$contador] = $row['descripcion'];
	$presentacion[$contador] = $row['presentacion'];
	$tipo_muestra[$contador] = $row['tipo'];
	$observaciones[$contador] = $row['observaciones'];
	$id_muestra[$contador] = $row['id_muestra'];
	$cantidad[$contador] = $row['cantidad'];

//	$result_codigo_fonasa=pg_query($conexion,"SELECT descripcion FROM fonasa WHERE codigo= '$des_iact[$contador]'");
	$result_codigo_fonasa=pg_query($conexion,"SELECT autocompletar FROM fonasa WHERE codigo= '$des_iact[$contador]' and autocompletar is not null  and precio > 0");

	$rows=pg_fetch_array($result_codigo_fonasa);
	$descripcion_codigo_fonasa[$contador]= $rows['autocompletar'];
    
//	while ($rows=pg_fetch_array($result_codigo_fonasa)){
//		$descripcion_codigo_fonasa[$contador].= $rows['descripcion'];
//	}
	
	if($contador == 0){
		
		$s1.='';	 
	}		 
	
	
		
	$contador ++;
}

for($i=0;$i<$contador;$i++){
	$s1.='<tr>';	
	  if($lateralidad[$i]==''){$lateralidad[$i]='No Corresponde';}
	  if($lateralidad[$i]=='I'){$lateralidad[$i]='Izquierda';}
	  if($lateralidad[$i]=='D'){$lateralidad[$i]='Derecha';} 
	  
	  if($tipo_muestra[$i]=="D"){$tipo_muestra[$i]="Diferida";} 
	  if($tipo_muestra[$i]=="R"){$tipo_muestra[$i]="Rápida";}
	  if($tipo_muestra[$i]=="C"){$tipo_muestra[$i]="Citología";}
	  
	  if($presentacion[$i]=="F"){$presentacion[$i]="Frasco";}
	  if($presentacion[$i]=="T"){$presentacion[$i]="Tubo";}
	  if($presentacion[$i]=="L"){$presentacion[$i]="Lámina";}	       
	  if($presentacion[$i]=="A"){$presentacion[$i]="Taco";}	       
	  if($presentacion[$i]=="B"){$presentacion[$i]="Bolsa";}
	  
	  if ($codigo[$i] != "" ){
	  	$organo_det = $codigo[$i].$descripcion[$i];
	  }else{
	  	$organo_det = $organo[$i];
	  }

	  $s1.="<td class='valor_listado_izq' width='195' >".$organo_det."</td>";	 
	  $s1.="<td class='valor_listado' width='30'>".$presentacion[$i]."</td>";	 
	  $s1.="<td class='valor_listado' width='50'>".$cantidad[$i]."</td>";	 
	  $s1.="<td class='valor_listado' width='80'>&nbsp;".$lateralidad[$i]."</td>";	   
	  $s1.="<td class='valor_listado' width='70' ".$columna_tipo.">".$tipo_muestra[$i]."</td>";	   
	  $s1.="<td class='valor_listado' width='50'>".$observaciones[$i]."&nbsp;</td>";
	  $s1.='<td class="valor_listado" width="30"><button  onclick="elimina_muestra('.$id_muestra[$i].')"><img src="imagenes/equis.png" style="width:15px; height:15px;" /></button></td>';	   
	  $s1.='</tr>';
}
	//$s1.='</table>';


/* ---------------------------------------------------------__ */
//$lista_organos = '';


/*
$lista_organos='<select id="id_organo_sol">
                 <option value=""></option>';
$queryFo=pg_Exec($conexion, "SELECT * FROM cieo_organos ORDER BY descripcion;");	
$codigo='';
while ($rowFo=pg_fetch_array($queryFo)){
  if ($rowFo['codigo']!=$codigo){
	$codigo=$rowFo['codigo'];  
	 $espacios=''; 	
  }	
  else
  {
	 $codigo='';
	 $espacios='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 
	  }
  $lista_organos.='<option value="'.$rowFo['codigo'].'">'.$espacios.' '.substr(ucwords(strtolower($rowFo['descripcion'])),0,50).' '.$codigo.'</option>';

}
$lista_organos.='</select>';
*/





               
//echo "SELECT codigo_fonasa FROM pesonalizar_fonasa WHERE id_unidad = ".$id_unidad_cambiar;               
/*               
$result = pg_query($conexion, "SELECT codigo_fonasa FROM pesonalizar_fonasa WHERE id_unidad = ".$id_unidad_cambiar) or die("ERRROR : ".pg_last_error());
$row=pg_fetch_array($result);
$codigos=explode(',',$row[0]);
			   
$sql="SELECT * FROM fonasa WHERE ( false ";

foreach($codigos as $nombre_campo => $valor){
  $sql.=" OR codigo = '".$valor."'";
} 
$sql.=" ) ";
			   		   
$queryFo=pg_Exec($conexion, $sql);	
$codigo='';
while ($rowFo=pg_fetch_array($queryFo)){
  if ($rowFo['codigo']!=$codigo){
	$codigo=$rowFo['codigo'];  
	 $espacios=''; 	
  }	
  else
  {
	 $codigo='';
	 $espacios='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 
	  }
  $lista_fonasa.='<option value="'.$rowFo['codigo'].'">'.$espacios.$codigo.' '.ucwords(strtolower($rowFo['descripcion'])).'</option>';

}
$lista_fonasa.='</select>';
*/




$lista_fonasa='<input id="id_fonasa_autocompletar" type="text"  style="width: 600px" onkeyup="buscar_fonasa()"/>';
$lista_fonasa.='<input  id="id_fonasa_sol" style="display:none;">';

//echo $id_examen.$cSql;

/*  <td>Fecha<?php espacios(1)?></td> */
//$s1.=' <table class="tabla_listado"  style="width:100%">';



if ($contador!='0'){

}

 if ($xperfil=='1'){  
 $s1.='<tr id="id_tr_muestra"  >  
		 <td class="valor_listado">
		 	<input id="id_organo_sol" type="text"   size="40" style="padding-left:7px" onkeypress=\'return pulso_enter(event,"id_presentacion_sol");\'>
		 </td>

		 <td class="valor_listado">
		 	<select id="id_presentacion_sol" onchange="set_cantidad()" style="padding-left:7px" onkeypress=\'return pulso_enter(event,"id_cantidad_sol");\'>
			   <option value="">Seleccione</option>
			   <option value="F">Frasco</option>
			   <option value="T">Tubo</option>       
		       <option value="L" selected="selected">Lámina</option>  
		       <option value="A">Taco</option>
			   <option value="B">Bolsa</option>
		  	</select>
		 </td>


		 <td class="valor_listado">
		 	<input id="id_cantidad_sol" type="text" size="3" style="padding-left:9px" value="1" disabled="disabled"  onkeypress=\'return pulso_enter(event,"id_lateralidad");\' />
		 </td>

		 <td class="valor_listado">
		 	<select id="id_lateralidad" style="padding-left:7px" onkeypress=\'return pulso_enter(event,"id_tipo_sol");\'>
			   <option value="">No Corresponde</option>  
			   <option value="I">Izquierda</option>
			   <option value="D">Derecha</option>       
		  	</select>
		 </td>
	 
	
		 <td class="valor_listado" '.$columna_tipo.'>
		 	<select id="id_tipo_sol" style="padding-left:7px" onkeypress=\'return pulso_enter(event,"id_observaciones_sol");\'>
		 	   <option value="">Seleccione</option>
			   <option value="D" selected="selected">Diferida</option>
			   <option value="R">Rápida</option>
			   <option value="C">Citología</option>       
			   
			 </select>
		 </td>
		 <td class="valor_listado">
		 	<textarea id="id_observaciones_sol" type="text" size="50" rows="5"  style="padding-left:7px"></textarea>
		 </td>   
		 
      <td class="valor_listado"><button onclick="elimina_adicionar()" id="id_elimina_adicionar" style="cursor:pointer" title="Eliminar muestra"><img src="imagenes/equis.png" style="width:15px; height:15px;" /></button></td>		 
	
	</tr>';
	
	//<option value="C">Citología</option>  
 //if ($contador!='0'){	
 $s1.='<tr>	
		 
         <td colspan="10" align="left">
	 			<button  onclick="graba_muestra(1)" style="cursor:pointer" title="Adicionar Muestra">Adicionar Muestra<img src="imagenes/Symbol-Add.png" style="width:20px; height:20px;" /></button>
	 	
		 </td>		 
		    
	 </tr>
';
 
	/*
	$s1.='		<tr>
		<td class="campoclic" colspan="100%">Prestación Fonasa</td>
	</tr> 
	<tr>
         <td colspan="100%" class="valor_listado">
		 	<div id="div_modo_busqueda_lista">
		 		'.$lista_fonasa.'
		 		<!--
		 		<input id="id_palabra_sol" type="text" size="17" />
	 			<button  onclick="filtro_fonasa()"><img src="imagenes/filtro.jpeg" style="width:15px; height:15px;" /></button>-->
	 			<button  onclick="graba_muestra()" ><img src="imagenes/Symbol-Check.png" style="width:15px; height:15px;" />Grabar Muestra</button>
	 			
		 	</div>
		 	
		 </td>		 
    </tr>';
	*/
 }
 $s1.='</table>';		  


echo $s1; 
?>