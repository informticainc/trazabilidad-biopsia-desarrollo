<?
include("../include/conectar.php");
include("../include/funciones_f.php");
	
foreach($_POST as $nombre_campo => $valor){
	$asignacion = "$" . $nombre_campo . "='" . $valor . "';";
	eval($asignacion);
	//echo $asignacion."<BR/>";
}
	
foreach($_GET as $nombre_campo => $valor){
	$asignacion = "$" . $nombre_campo . "='" . $valor . "';";
	eval($asignacion);	
	//echo $asignacion."<BR/>";
}

$result = pg_query($conexion, "SELECT * FROM especialidad WHERE descripcion  = '$id_nombre_especialidad'");
// echo "SELECT * FROM especialidad WHERE descripcion  = '$id_nombre_especialidad'";

$contador = 0;
while($row = pg_fetch_array($result)){
	
	$id_especialidad[$contador] = $row['id_especialidad'];
	$descripcion[$contador] = $row['descripcion'];
	
	$contador ++;
}
?>

<script type="text/javascript">
	
	function editar_especialidad(valor){
		$('#texto_fijo').css('display','none');
		$('#icono_editar').css('display','none');
		$('#input_modificable').css('display','block');
		$('#icono_guardar').css('display','block');
	}
	
	function guardar_edicion_de_especialidad(valor){
		var id_especialidad = valor;
		var id_especialidad_editar = $("#id_especialidad_editar").val();	
		
		$.ajax({
				type		: "POST",
				url			: "ajax/editar_datos_especialidad.ajax.php",
				data		: "id_especialidad="+id_especialidad+
							  "&id_especialidad_editar="+id_especialidad_editar+
							  "&random="+Math.random(),
				beforeSend: function(){
					var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
					dialog.dialog('open');
				},			  
				success	: function(datos){
					var id_especialidad_editar = $("#id_especialidad_editar").val();		
					
					if(datos == 1){
						$('#id_nombre_especialidad').val(id_especialidad_editar);
						$('#texto_fijo').html(id_especialidad_editar);
						var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
						dialog.dialog('close');
						alert("Se actualizado correctamente");
					}else{
						var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
						dialog.dialog('close');
						alert("Ha ocurrido un problema comuniquese con informatica");
					}
					
					$('#texto_fijo').css('display','block');
					$('#icono_editar').css('display','block');
					$('#input_modificable').css('display','none');
					$('#icono_guardar').css('display','none');
				}		  					
		 	});
	}

	function eliminar_especialidad(valor){
		
		var id_especialidad = valor;
		
		$.ajax({
				type		: "POST",
				url			: "ajax/eliminar_datos_especialidad.ajax.php",
				data		: "id_especialidad="+id_especialidad+
							  "&random="+Math.random(),
				beforeSend: function(){
					var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
					dialog.dialog('open');
				},			  
				success	: function(datos){
					if(datos == 1){
						$('#id_nombre_especialidad').val('');
						$('#div_listado_especialidad').css('display','none');
						var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
						dialog.dialog('close');
						alert("Se ha eliminado la especialidad correctamente");
					}else{
						var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
						dialog.dialog('close');
						alert("Ha ocurrido un problema comuniquese con informatica");
					}
				}
			});		
		
	}
	
</script>

<table class="listado">
	<tr>
		<td>
			<table class="resultado">
				<tr>
					<td class="descripcion seleccion">
						Especialidad
					</td>
					<td class="descripcion seleccion">
						Editar
					</td>
					<td class="descripcion seleccion">
						Eliminar
					</td>
				</tr>
				<? for($i=0; $i<$contador;$i++){?>
				<tr class="seleccion">
					<td class="valor" >
						<span id="texto_fijo" >
							<?= $descripcion[$i];?>
						</span>
						<span id="input_modificable" style="display: none;" >
							<input type="text" id="id_especialidad_editar" value="<?= $descripcion[$i]; ?>" >
						</span>
					</td>
					<td class="accion">
						<span id="icono_editar">
							<img src="imagenes/icono/Linux/ICO/Edit.ico" onclick="editar_especialidad(<?= $id_especialidad[$i] ?>)">
						</span>
						<span id="icono_guardar" style="display: none;">
							<img src="imagenes/icono/Linux/ICO/Save.ico" onclick="guardar_edicion_de_especialidad(<?= $id_especialidad[$i] ?>)" >
						</span>
					</td>
					<td class="accion">
						<img src="imagenes/icono/Linux/ICO/Symbol-Delete.ico" onclick="eliminar_especialidad(<?= $id_especialidad[$i] ?>)">
					</td>
				</tr>	
				<?}?>
			</table>	
		</td>
	</tr>
</table>
