<? include('include/conectar.php') ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<title>:: Mantenedor Biopsias ::</title>

    	<!-- Css sistemas-->
    	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />  
    	
        <!-- Css jquery ui--> 
        <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" media="all"/>
        
    	<!-- JS jquery y jquery ui -->		
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
        
        <!-- Plugin Valida Rut -->
        <script type="text/javascript" src="js/jquery.Rut.js"></script>
        <script type="text/javascript" src="js/jquery.Rut.min.js"></script>
        
        <!-- Plugin Para mantener el lugar Del ajax -->
        <script type="text/javascript" src="js/sammy-0.5.4.min.js"></script>
        
        
        <script type="text/javascript">
        
        	$(function(){
		
				$('#id_run_medico').Rut({
				  on_error: function(){ /*alert('El RUN ingresado esta incorrecto');*/$("#id_run_medico").removeClass('validado'); $("#id_run_medico").addClass('requerido');  $('#id_run_medico').val(''); return; },
				  on_success: function(){$("#id_run_medico").addClass('validado');}
				});
			});
			
			
        	$(function() {
        		// AUTOCOMPLETAR ESPECIALIDAD
	        	
	        	$('#id_unidad').autocomplete(
				{
				    source: "ajax/busqueda_autocompletar_unidad.ajax.php",
				    minLength: 1,
					select: function( event, ui ) {
						
							var descripcion_unidad = ui.item.label;
							$('#id_unidad').val(descripcion_unidad);
							
							$.ajax({
									type		: "POST",
									url			: "ajax/buscar_idregistrounidad.ajax.php",
									data		: "descripcion_unidad="+descripcion_unidad+
												  "&random="+Math.random(),
									beforeSend: function(){
										//var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
										//dialog.dialog('open');
									},			  
									success	: function(datos){
										
										var id_registro_unidad  = $.trim(datos); 
										
										$('#id_registro_unidad').val(id_registro_unidad);
									}		  					
							 	});
							 
                            return false;
                    },
        			change:     function(event,ui) {
           			}
				});
				
				
				
				//AUTOCOMPLETAR RUN
				$('#id_run_medico').autocomplete(
				{
				    source: "ajax/busqueda_autocompletar_run_medico.ajax.php",
				    minLength: 1,
					select: function( event, ui ) {
						
							var nom_apell = ui.item.label;
							
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							
							var	 nom_apell_split = nom_apell.split(",")
							
							
							$('#id_apell_pat_medico').val(nom_apell_split[2]);
							$('#id_apell_mat_medico').val(nom_apell_split[3]);
							$('#id_nombre_medico').val(nom_apell_split[1]);
							$('#id_run_medico').val(nom_apell_split[0]);
							
							var run_medico = nom_apell_split[0];
							
							$.ajax({
									type		: "POST",
									url			: "ajax/buscar_especialidad_idmedico.ajax.php",
									data		: "run_medico="+run_medico+
												  "&random="+Math.random(),
									beforeSend: function(){
										//var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
										//dialog.dialog('open');
									},			  
									success	: function(datos){
										
										var id_registro_medico  = $.trim(datos); 
										
										$('#id_registro_medico').val(id_registro_medico);
									}		  					
							 	});
							 
                            return false;
                    },
        			change:     function(event,ui) {
           			}	
									
				});
				
				
				//AUTOCOMPLETAR NOMBRE MEDICO
				$('#id_nombre_medico').autocomplete(
				{
				    source: "ajax/busqueda_autocompletar_nombre_medico.ajax.php",
				    minLength: 1,
					select: function( event, ui ) {
						
							var nom_apell = ui.item.label;
							
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							
							var	 nom_apell_split = nom_apell.split(",")
							
							$('#id_run_medico').val(nom_apell_split[0]);
							$('#id_apell_pat_medico').val(nom_apell_split[2]);
							$('#id_apell_mat_medico').val(nom_apell_split[3]);
							$('#id_nombre_medico').val(nom_apell_split[1]);
							
							var run_medico = nom_apell_split[0];
							
							$.ajax({
									type		: "POST",
									url			: "ajax/buscar_especialidad_idmedico.ajax.php",
									data		: "run_medico="+run_medico+
												  "&random="+Math.random(),
									beforeSend: function(){
										//var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
										//dialog.dialog('open');
									},			  
									success	: function(datos){
										
										var id_registro_medico  = $.trim(datos); 
										
										$('#id_registro_medico').val(id_registro_medico);
									}		  					
							 	});
							 	
                            return false;
                    },
        			change:     function(event,ui) {
           			}	
									
				});
				
				
				//Apellido Paterno
				$('#id_apell_pat_medico').autocomplete(
				{
				    source: "ajax/busqueda_autocompletar_apellido_paterno_medico.ajax.php",
				    minLength: 1,
					select: function( event, ui ) {
						
							var nom_apell = ui.item.label;
							
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							
							var	 nom_apell_split = nom_apell.split(",")
							
							
							$('#id_apell_pat_medico').val(nom_apell_split[2]);
							$('#id_apell_mat_medico').val(nom_apell_split[3]);
							$('#id_nombre_medico').val(nom_apell_split[1]);
							$('#id_run_medico').val(nom_apell_split[0]);
							
							var run_medico = nom_apell_split[0];
							
							$.ajax({
									type		: "POST",
									url			: "ajax/buscar_especialidad_idmedico.ajax.php",
									data		: "run_medico="+run_medico+
												  "&random="+Math.random(),
									beforeSend: function(){
										//var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
										//dialog.dialog('open');
									},			  
									success	: function(datos){
										
										var id_registro_medico  = $.trim(datos); 
										
										$('#id_registro_medico').val(id_registro_medico);
									}		  					
							 	});
							 
                            return false;
                    },
        			change:     function(event,ui) {
           			}	
									
				});
				
				//Apellido Materno
				$('#id_apell_mat_medico').autocomplete(
				{
				    source: "ajax/buscar_autocompletar_apellido_materno_medico.ajax.php",
				    minLength: 1,
					select: function( event, ui ) {
						
							var nom_apell = ui.item.label;
							
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							var nom_apell = nom_apell.replace(' ', ',');
							
							var	 nom_apell_split = nom_apell.split(",")
							
							$('#id_apell_pat_medico').val(nom_apell_split[2]);
							$('#id_apell_mat_medico').val(nom_apell_split[3]);
							$('#id_nombre_medico').val(nom_apell_split[1]);
							$('#id_run_medico').val(nom_apell_split[0]);
							
							var run_medico = nom_apell_split[0];
							
							$.ajax({
									type		: "POST",
									url			: "ajax/buscar_especialidad_idmedico.ajax.php",
									data		: "run_medico="+run_medico+
												  "&random="+Math.random(),
									beforeSend: function(){
										//var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
										//dialog.dialog('open');
									},			  
									success	: function(datos){
										
										var id_registro_medico  = $.trim(datos); 
										
										$('#id_registro_medico').val(id_registro_medico);
									}		  					
							 	});
							 
                            return false;
                    },
        			change:     function(event,ui) {
           			}	
									
				});
			});	
			
        	var opt_dialogo_cargando = {
										autoOpen: false,
										modal: true,
										width: 100,
										height: 100,
										cache:false,
										resizable:false,
										modal:true,
										position: 'center',
										open: function(event, ui) { 
												$(".ui-dialog-titlebar-close").hide();
												$(".ui-dialog-titlebar").hide();    
											}
										}
        	/*
        	function guardar_datos_especialidad(){
        		
        		var id_nombre_especialidad = $("#id_nombre_especialidad").val();
        		
        		if(id_nombre_especialidad == ""){
        			alert("Debe insertar una especialidad");return;
        		}
        		
        		$.ajax({
						type		: "POST",
						url			: "ajax/grabar_datos_especiliadad.ajax.php",
						data		: "id_nombre_especialidad="+id_nombre_especialidad+
									  "&random="+Math.random(),
						beforeSend: function(){
							var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
							dialog.dialog('open');
						},			  
						success	: function(datos){
							var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
							dialog.dialog('close');
							
							alert(datos);
							
							$.ajax({
									type		: "POST",
									url			: "ajax/actualizar_span_especialidad.ajax.php",
									success	: function(datos){
										
										alert(datos);
										$("#id_actualizar_especialidad").html(datos);
									}		  					
							 	});
						}		  					
				 	});
			}
			
			function buscar_datos_especialidad(){
				
				var id_nombre_especialidad = $("#id_nombre_especialidad").val();
        		
        		if(id_nombre_especialidad == ""){
        			alert("Debe insertar una especialidad para realizar la busqueda");return;
        		}
        		
        		$.ajax({
						type		: "POST",
						url			: "ajax/buscar_datos_especialidad.ajax.php",
						data		: "id_nombre_especialidad="+id_nombre_especialidad+
									  "&random="+Math.random(),
						beforeSend: function(){
							var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
							dialog.dialog('open');
						},			  
						success	: function(datos){
							$('#div_listado_especialidad').css('display','block')
							var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
							dialog.dialog('close');
							
							$('#div_listado_especialidad').html(datos);
						}		  					
				 	});
			}
			*/
			function guardar_datos_medico(){
				
				
				var id_run_medico 		= $('#id_run_medico').val();
				var id_nombre_medico 		= $('#id_nombre_medico').val();
				var id_apell_pat_medico	 	= $('#id_apell_pat_medico').val();
				var id_apell_mat_medico 	= $('#id_apell_mat_medico').val();
				//var id_especialidad_medico 	= $('#id_especialidad_medico').val();
				
				var id_run_medico  = $.trim(id_run_medico);
				
				var error = false;
				
				if(id_run_medico == ""){
					$("#id_run_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_run_medico").addClass('validado');
					$("#id_run_medico").removeClass('requerido')
					
				}
				if(id_nombre_medico == ""){
					$("#id_nombre_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_nombre_medico").addClass('validado');
					$("#id_nombre_medico").removeClass('requerido')
					
				}
				if(id_apell_pat_medico == ""){
					$("#id_apell_pat_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_apell_pat_medico").addClass('validado');
					$("#id_apell_pat_medico").removeClass('requerido');
				}
				
				if(id_apell_pat_medico != ""){
					$("#id_apell_pat_medico").addClass('validado');
				}
				
				if( error == true){				
					
					alert('Los datos marcados en rojo son de caracter obligatorio');
				}else{
					
					$.ajax({
							type		: "POST",
							url			: "ajax/grabar_datos_medicos.ajax.php",
							data		: "id_nombre_medico="+id_nombre_medico+
										  "&id_apell_pat_medico="+id_apell_pat_medico+
										  "&id_apell_mat_medico="+id_apell_mat_medico+
										  "&id_run_medico="+id_run_medico+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								
								//alert(datos);return;
								
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								if(datos == 3){alert("Este rut ya existe en la base de datos"); return;}
								if(datos == 1){
									alert( "Se ha insertado los datos correctamente");
									if ($("#div_listado_datos_medico").css("display") == "block" ){
										$("#div_listado_datos_medico").load("ajax/buscar_datos_medicos.ajax.php");
									}
								}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
							}		  					
					 	});
				}	 	
			}
			
			function editar_datos_medico(){
				var id_registro_medico 		= $('#id_registro_medico').val();
				var id_run_medico 			= $('#id_run_medico').val();
				var id_nombre_medico 		= $('#id_nombre_medico').val();
				var id_apell_pat_medico	 	= $('#id_apell_pat_medico').val();
				var id_apell_mat_medico 	= $('#id_apell_mat_medico').val();
				//var id_especialidad_medico 	= $('#id_especialidad_medico').val();
				
				var error = false;
				
				if(id_run_medico == ""){
					$("#id_run_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_run_medico").addClass('validado');
					$("#id_run_medico").removeClass('requerido')
				}
				if(id_nombre_medico == ""){
					$("#id_nombre_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_nombre_medico").addClass('validado');
					$("#id_nombre_medico").removeClass('requerido')
				}
				if(id_apell_pat_medico == ""){
					$("#id_apell_pat_medico").addClass('requerido');
					var error = true;
				}else{
					$("#id_apell_pat_medico").addClass('validado');
					$("#id_apell_pat_medico").removeClass('requerido')
				}
				
				if(id_apell_pat_medico != ""){
					$("#id_apell_pat_medico").addClass('validado');
				}
				
				
				if( error == true){
					alert('Los datos marcados en rojo son de caracter obligatorio');
				}else{
				
					$.ajax({
							type		: "POST",
							url			: "ajax/editar_datos_medicos.ajax.php",
							data		: "id_nombre_medico="+id_nombre_medico+
										  "&id_apell_pat_medico="+id_apell_pat_medico+
										  "&id_apell_mat_medico="+id_apell_mat_medico+
										  "&id_run_medico="+id_run_medico+
										  "&id_registro_medico="+id_registro_medico+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								if(datos == 1){
									alert( "Se han editado los datos correctamente");
									
										if ( $("#div_listado_datos_medico").css("display") == "block" ){
											$("#div_listado_datos_medico").load("ajax/buscar_datos_medicos.ajax.php");
										}
										
									}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
							}		  					
					 	});
				}
			}
			
			function eliminar_datos_medico(){
				
				var id_registro_medico = $('#id_registro_medico').val();
				$.ajax({
							type		: "POST",
							url			: "ajax/eliminar_datos_medico.ajax.php",
							data		: "id_registro_medico="+id_registro_medico+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								
								if(datos == 1){
									alert( "Se han eliminado los datos correctamente");
									$('#id_registro_medico').val("");
									$('#id_run_medico').val("");
									$('#id_nombre_medico').val("");
									$('#id_apell_pat_medico').val("");
									$('#id_apell_mat_medico').val("");
									
									if ($("#div_listado_datos_medico").css("display") == "block" ){
										$("#div_listado_datos_medico").load("ajax/buscar_datos_medicos.ajax.php");
									}
								}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
								
								
							}		  					
					 	});
					 	
				
			}
			
			
			function guardar_datos_unidad(){
				
				var id_unidad 		= $('#id_unidad').val();
				var id_unidad  = $.trim(id_unidad); 
				
				var error = false;
				
				if(id_unidad == ""){
					$("#id_unidad").addClass('requerido');
					var error = true;
				}else{
					$("#id_unidad").addClass('validado');
					$("#id_unidad").removeClass('requerido')
					var error = false;
				}
				
				if( error == true){
					alert('Los datos marcados en rojo son de caracter obligatorio');
				}else{
				
					$.ajax({
							type		: "POST",
							url			: "ajax/grabar_datos_unidad.ajax.php",
							data		: "id_unidad="+id_unidad+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								if(datos == 3){alert("Esta unidad ya existe en la base de datos"); return;}
								if(datos == 1){
									alert( "Se ha insertado los datos correctamente");
									
									if ( $("#div_listado_unidades").css("display") == "block" ){
											$("#div_listado_unidades").load("ajax/buscar_datos_unidad.ajax.php");
									}		
								}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
							}		  					
					 	});
				}
			}
			
			function eliminar_datos_unidad(){
				
				var id_unidad = $('#id_unidad').val();
				var id_unidad  = $.trim(id_unidad); 
				
				$.ajax({
							type		: "POST",
							url			: "ajax/eliminar_datos_unidad.ajax.php",
							data		: "id_unidad="+id_unidad+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								
								if(datos == 1){
									alert( "Se han eliminado la unidad correctamente");
									$('#id_unidad').val("");
									
									if ( $("#div_listado_unidades").css("display") == "block" ){
											$("#div_listado_unidades").load("ajax/buscar_datos_unidad.ajax.php");
									}
									
								}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
								
								
							}		  					
					 	});
			}
			
			function editar_datos_unidad(){
				var id_registro_unidad = $('#id_registro_unidad').val();
				var id_unidad = $('#id_unidad').val();
				var id_registro_unidad  = $.trim(id_registro_unidad); 
				
				var error = false;
				
				if(id_unidad == ""){
					$("#id_unidad").addClass('requerido');
					var error = true;
				}else{
					$("#id_unidad").addClass('validado');
					$("#id_unidad").removeClass('requerido')
					var error = false;
				}
				
				if( error == true){
					alert('Los datos marcados en rojo son de caracter obligatorio');
				}else{
				
					$.ajax({
							type		: "POST",
							url			: "ajax/editar_datos_unidad.ajax.php",
							data		: "id_registro_unidad="+id_registro_unidad+
										  "&id_unidad="+id_unidad+
										  "&random="+Math.random(),
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								if(datos == 1){
									alert( "Se ha editado la unidad correctamente");
									if ( $("#div_listado_unidades").css("display") == "block" ){
											$("#div_listado_unidades").load("ajax/buscar_datos_unidad.ajax.php");
									}
								}else{
									alert("Ha ocurrido un problema, comuniquese con Informatica");
								}
							}		  					
					 	});
				}
				
			}
			
			function desplegar_unidades(){
				if ( $("#div_listado_unidades").css("display") == "none" ){
	        		$.ajax({
							type		: "POST",
							url			: "ajax/buscar_datos_unidad.ajax.php",
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								$('#div_listado_unidades').css('display','block')
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								$('#div_listado_unidades').html(datos);
							}		  					
					 	});
				}else{
					$("#div_listado_unidades").css("display","none");
				}		 	
			}
			
			function desplegar_medicos(){
				
				if ( $("#div_listado_datos_medico").css("display") == "none" ){
				
		    		$.ajax({
							type		: "POST",
							url			: "ajax/buscar_datos_medicos.ajax.php",
							beforeSend: function(){
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('open');
							},			  
							success	: function(datos){
								$('#div_listado_datos_medico').css('display','block')
								var dialog = $("#div_cargando").dialog(opt_dialogo_cargando);
								dialog.dialog('close');
								
								$('#div_listado_datos_medico').html(datos);
							}		  					
					 	});
				}else{
					$("#div_listado_datos_medico").css("display","none");
				}	 	
			}
			
			
function volveratras()
  {
  window.history.back()
  }
        </script>
        
  </head>
  <body>
  	
  	<div id="center">		
		<div id="div_mostrar"></div>
	  	<div id="div_contenedor" class="home">
	  		<h1>Mantenedor Sistema Biopsias</h1>
	  		<button onclick="volveratras()">Atras</button>
			<h2>  Ingreso, Edicion y Busqueda De Medico</h2>
				<table class="formulario">
					<tr>
						<td class="capsula">
							<table>
								<tr>
		  							<td class="texto">
				  						RUN
				  					</td>
				  					<td class="contenido">
				  						<input type="text" id="id_run_medico" />
				  						<input type="text" id="id_registro_medico" style="display: none;">
				  					</td>
			  					</tr>
							</table>	
						</td>
						<td class="capsula">
							<table>
								<tr>
		  							<td class="texto">
				  						Nombre
				  					</td>
				  					<td class="contenido">
				  						<input type="text" id="id_nombre_medico" />
				  					</td>
			  					</tr>
							</table>	
						</td>
						<td class="capsula">
							<table>
								<tr>
		  							<td class="texto">
				  						Apellido Paterno
				  					</td>
				  					<td class="contenido">
				  						<input type="text" id="id_apell_pat_medico" />
				  					</td>
			  					</tr>
							</table>	
						</td>
						<td class="capsula">
							<table>
								<tr>
		  							<td class="texto">
				  						Apellido Materno
				  					</td>
				  					<td class="contenido">
				  						<input type="text" id="id_apell_mat_medico" />
				  					</td>
			  					</tr>
							</table>	
						</td>
					
					<tr>
	  					<td class="botonera" colspan="100%">
							<button onclick="guardar_datos_medico();">
						   		Guardar Nuevo Medico
						    </button>
						    <button onclick="editar_datos_medico();">
						   		Editar
						    </button>
						    <button onclick="eliminar_datos_medico();">
						   		Eliminar
						    </button>
						     <button onclick="desplegar_medicos();">
						   		Desplegar Todos Los Medicos
						    </button>
						    
	  					</td>
					</tr>
				</table>
				<div id="div_listado">
					<div id="div_listado_datos_medico" style="display: none">
					
					</div>
				</div>	
				
				
				<!-- UNIDADES -->
				
				<h2> Ingreso, Edicion y  Busqueda De Unidades</h2>
				<table class="formulario">
					<tr>
						<td class="capsula">
							<table>
								<tr>
		  							<td class="texto">
				  						Unidades
				  					</td>
				  					<td class="contenido">
				  						<input type="text" id="id_unidad"  />
				  						<input type="text" id="id_registro_unidad" style="display: none;"/>
				  					</td>
			  					</tr>
							</table>	
						</td>
					</tr>
					<tr>
	  					<td class="botonera" colspan="100%">
							<button onclick="guardar_datos_unidad();">
						   		Guardar Nueva Unidad
						    </button>
						    <button onclick="editar_datos_unidad();">
						   		Editar
						    </button>
						    <button onclick="eliminar_datos_unidad();">
						   		Eliminar
						    </button>
						    <button onclick="desplegar_unidades();">
						   		Desplegar Todas Las Unidades
						    </button>
	  					</td>
					</tr>
				</table>
				<div id="div_listado">
					<div id="div_listado_unidades" style="display: none">
					
						<!-- 
							¡¡¡  DIV LISTADO UNIDADES !!!
						-->
					
					</div>
				</div>	
			</div>	
	  	</div>
  	</div>	
  </body>
</html>  
<div style="display: none;">
<? include("dialog/cargando.php");?>
</div>