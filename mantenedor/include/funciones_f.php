<?php

// Fecha: ene-11-2012
// Descripción: esta función retorna una cadena de texto a partir de una tabla, con las opciones para
// generar una lista desplegable.
// Parametros:
// $sSql  "Instrucciones sql"
// $cValue llave principal de la tabla (la que se asignara a la variable, esta no se muestra al Usuario)
// $cLlave1 Primer campo de la tabla que desea visualizar
// $cLlave2 Segundo campo de la tabla que desea visualizar

//Para Mysql


function edad($edad){
list($anio,$mes,$dia) = explode("-",$edad);
$anio_dif = date("Y") - $anio;
$mes_dif = date("m") - $mes;
$dia_dif = date("d") - $dia;
if ($dia_dif < 0 || $mes_dif < 0)
$anio_dif--;
return $anio_dif;
}

Function lista_desplegable ($cSql,$cValue,$cLlave1,$cLlave2,$ccondicion){
$result = mysql_query($cSql);
$opciones="";
global $cLlave;

//alert($ccondicion);
while ($row = mysql_fetch_array($result)) {
if (isset($cLlave2) && $cLlave2!="") {
   $cResto=$row[$cLlave1]." ".$row[$cLlave2]."</option>\n";}
else {
   $cResto=$row[$cLlave1]."</option>\n"; }
//		alert($cLlave." -> ".$row[$cValue]." ".gettype($cLlave));   
     if ($cLlave == $row[$cValue] ){
        $opciones=$opciones. "<option value=".$row[$cLlave1]." selected>".$cResto; 
      } else {
         $opciones=$opciones. "<option value=".$row[$cValue]." >".$cResto; 
    }
}
return $opciones;
}

//Para postgres
Function lista_desplegablepg ($cSql,$cValue,$cLlave1,$cLlave2,$ccondicion){

//$result = mysql_query($cSql);
$opciones="";
global $cLlave;
global $conexion;
$query = pg_Exec($conexion, $cSql);
while ($row = pg_fetch_array($query)) {
 if (isset($cLlave2) && $cLlave2!="") {
    $cResto=$row[$cLlave1]." ".$row[$cLlave2]."</option>\n";}
 else {
   $cResto=$row[$cLlave1]."</option>\n"; }
     if (rtrim($cLlave) == rtrim($row[$cValue]) ){
        $opciones=$opciones. "<option value=".$row[$cValue]." selected>".$cResto; 
      } else {
         $opciones=$opciones. "<option value=".$row[$cValue]." >".$cResto; 
 }
}
return $opciones;
}



//Retorna el valor de un campo especifico

//Mysql
Function traercampo ($cSql){
$result = mysql_query($cSql);
$respuesta="";
global $cLlave;
while ($row = mysql_fetch_array($result)) {
   $respuesta= $row[0]; 
}
return $respuesta;
}

//Postgres
Function pg_traercampo ($cSqlp,$conexion){
	
$query = pg_Exec($conexion, $cSqlp);
$respuesta="";

//if (!isset($ncampos)){
 //$ncampos=1;
//}

while ($row = pg_fetch_array($query)) {
   $respuesta= $row[0];
  // alert(sizeof($row));
  // if (sizeof($row)>1){
  //   $respuesta= $row[0].$row[1]; 	   
  // }
   
}
return $respuesta;
}


Function msg_href($sarta,$href){
	
 echo "<script language='JavaScript'>
 alert('".$sarta."');".$href."</script>";	
}

Function alert($sarta){
	
 echo "<script language='JavaScript'>
 alert('".$sarta."');
 </script>";	
}



function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) 
  {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case  "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}	

function haceselected($cvble,$coption){

 if (rtrim($cvble) == rtrim($coption))	{
  return " selected ";	
 }
 else{
  return "";	 
 }
}

function cambiarFormatoFecha($fecha){
	if ($fecha){ 
    list($anio,$mes,$dia)=explode("-",$fecha);
		if ($dia ==''){
           return $fecha;			
		}
		else{
	      return $dia."/".$mes."/".$anio;		
		}
    
	}
	return "";
} 

function resta_fechas($fecha1,$fecha2)
	{
		  if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
				  list($dia1,$mes1,$año1)=split("/",$fecha1);
		  if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
				  list($dia1,$mes1,$año1)=split("-",$fecha1);
			if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
				  list($dia2,$mes2,$año2)=split("/",$fecha2);
		  if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
				  list($dia2,$mes2,$año2)=split("-",$fecha2);
			$dif = mktime(0,0,0,$mes1,$dia1,$año1) - mktime(0,0,0,$mes2,$dia2,$año2);
		  $ndias=floor($dif/(24*60*60));
		  return($ndias);
	}
function dv($r){$s=1;for($m=0;$r!=0;$r/=10)$s=($s+$r%10*(9-$m++%6))%11;

return chr($s?$s+47:75);
}



function normaliza ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}
?>


