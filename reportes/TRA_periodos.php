
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="CSS/Estilo_principal.css" rel="stylesheet" type="text/css" media="all"/>
<title>Trazabilidad de biopsias</title>
<script language="javascript">


function volver()
{
parent.location="TRA_reportes.php"
}
</script>
</head>

<body>
<div id="contenedor">


<h1> Estados de Biopsias: </h1>
<br/>

<form action="TRA_periodos.php" method="post" class="registro" >

DESDE &nbsp;<input type="date" name="fecha1" />&nbsp;&nbsp;
HASTA &nbsp;<input type="date" name="fecha2" />&nbsp;&nbsp;


<input type="submit" class="boton_1" value="consultar" align="leftt" />  
* dd-mm-aaaa
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;



<!--<input type="reset" class="boton_1" value="Limpiar">-->
</form>
<br/>
<input type="submit" class="boton_1" value="Volver" Onclick="volver()"/>
<BR/>
<br/>
<br/>


<!--<input type=image src="Imagen/Excel.jpg"  Onclick="window.location = 'Exportar_excel.php'" value="Exportar"/>-->


<?php

$fecha1 = $_REQUEST['fecha1'];
$fecha1=date("Y-m-d",strtotime($fecha1));

$fecha2 = $_REQUEST['fecha2'];
$fecha2=date("Y-m-d",strtotime($fecha2));

?>
<a href="../reportes/Excel_periodos.php?fecha1=<?php echo $fecha1; ?>&amp;fecha2=<?php echo $fecha2; ?>"><img src="../reportes/Imagen/Excel.jpg" alt="Exportación a Excel"> </a>
<br/>



<?php

include ('../config/conectar_bd.php');
//include ('include/conexion.php');

//$query2 = "SELECT * FROM muestras WHERE fecha BETWEEN '$fecha1' AND '$fecha2'";

$Sqlquery = "select mu.numero_examen, pa.nombres, pa.primer_apellido, pa.segundo_apellido, pa.rut, pa.dv, pa.edad, org.descripcion, mu.fecha, inf.fecha_entrega_informe, inf.fecha_archiva_informe, ex.diagnosticos_clinicos
from pacientes as pa
	left join examenes as ex on pa.rut = ex.rut_paciente
		left join muestras as mu using (id_examen)
			left join informes as inf using (id_examen)
				left join cieo_organos as org on org.codigo = mu.cod_cieo
	where mu.fecha BETWEEN '$fecha1' and '$fecha2'
	order by fecha asc";

//echo $Sqlquery;

$query = pg_query($conexion, $Sqlquery);


if (!$query ) { 
echo "An error occurred.\n" ; 
exit; 
} 



?>

<table width="1000" border="1" align="center">

  <tr align="center"bgcolor="#FF9900" >
    <td style="font:bold">N°</td>
    <td style="font:bold">N° biopsia</td>
    <td style="font:bold">Nombres</td>
    <td style="font:bold">Apellido Paterno</td>
    <td style="font:bold">Apellido Materno</td>
    <td style="font:bold">Rut</td>
    <td style="font:bold">Fecha Nacimiento</td>
    <td style="font:bold">Edad</td>
    <td style="font:bold">&Oacute;rgano</td>
    <td style="font:bold">Fecha Ingreso</td>
    <td style="font:bold">Fecha Informe</td>
    <td style="font:bold">Fecha despacho</td>
    <td style="font:bold">Diagn&oacute;stico</td>
    
   
  </tr>

<?php
$cont=1;
while($row = pg_fetch_array($query,NULL,PGSQL_ASSOC))
{
	//$id_ambu = $_REQUEST["id_ambu"];
//	
//	$consulta2 = oci_parse($conn, "Select * from tab_paciente where id_ambulatorio=$id_ambu");
//	$datos2 = oci_execute($consulta2);

?>

<tr align='center' >

    <td><?php echo $cont++; ?></td>
    <td><?php echo $row['numero_examen']; ?></td>
    <td><?php echo $row['nombres']; ?></td>
    <td><?php echo $row['primer_apellido']; ?></td>
    <td><?php echo $row['segundo_apellido']; ?></td>
    <td><?php echo $row['rut']?>-<?php echo $row['dv']; ?></td>
    <td><?php echo $row['fecha_nacimiento']; ?></td>
    <td><?php echo $row['edad']; ?></td>
    <td><?php echo $row['descripcion']; ?></td>
    <td><?php echo $row['fecha']; ?></td>
    <td><?php echo $row['fecha_entrega_informe']; ?></td>
    <td><?php echo $row['fecha_archiva_informe']; ?></td>
    <td><?php echo $row['diagnosticos_clinicos']; ?></td>
<?php
}
?>

 </tr>
   </table>
 
 </div>
</body>
</html>
