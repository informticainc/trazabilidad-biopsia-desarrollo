
<?php
header("Content-type: application/vnd.ms-excel;charset=utf-8");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=Reporte.xls");

?>


<html>
<body>

<h1> Informe Productividad </h1>
<br/>

<?php

$fecha1 = $_GET['fecha1'];
$fecha1=date("Y-m-d",strtotime($fecha1));

$fecha2 = $_GET['fecha2'];
$fecha2=date("Y-m-d",strtotime($fecha2));

include ('../config/conectar_bd.php');

//$query2 = "SELECT * FROM muestras WHERE fecha BETWEEN '$fecha1' AND '$fecha2'";

$Sqlquery = "select mu.numero_examen, pa.nombres, pa.primer_apellido, pa.segundo_apellido, pa.rut, pa.dv, pa.numero_ficha, org.descripcion, mu.fecha, mu.presentacion, mu.lateralidad, mu.cantidad, mu.tipo, mu.observaciones, inf.fecha_entrega_informe, inf.fecha_archiva_informe, ex.diagnosticos_clinicos
from pacientes as pa
	left join examenes as ex on pa.rut = ex.rut_paciente
		left join muestras as mu using (id_examen)
			left join informes as inf using (id_examen)
				left join cieo_organos as org on org.codigo = mu.cod_cieo
	where mu.fecha BETWEEN '$fecha1' and '$fecha2'
	order by fecha asc";

//echo $Sqlquery;

$query = pg_query($conexion, $Sqlquery);


if (!$query ) { 
echo "An error occurred.\n" ; 
exit; 
} 



?>

<table width="1000" border="1" align="center">

  <tr align="center"bgcolor="#FF9900" >
    <td style="font:bold">N</td>
    <td style="font:bold">N biopsia</td>
    <td style="font:bold">Nombres</td>
    <td style="font:bold">Apellido Paterno</td>
    <td style="font:bold">Apellido Materno</td>
    <td style="font:bold">Rut</td>
    <td style="font:bold">Ficha</td>
    <td style="font:bold">Fecha Ingreso</td>
    <td style="font:bold">&Oacute;rgano</td>
    <td style="font:bold">Presentaci&oacute;n</td>
    <td style="font:bold">Lateralidad</td>
    <td style="font:bold">Cantidad</td>
    <td style="font:bold">Tipo</td>
    <td style="font:bold">Observaciones</td>
   
  </tr>

<?php
$cont=1;
while($row = pg_fetch_array($query,NULL,PGSQL_ASSOC))
{
	//$id_ambu = $_REQUEST["id_ambu"];
//	
//	$consulta2 = oci_parse($conn, "Select * from tab_paciente where id_ambulatorio=$id_ambu");
//	$datos2 = oci_execute($consulta2);

?>

<tr align='center' >

    <td><?php echo $cont++; ?></td>
    <td><?php echo $row['numero_examen']; ?></td>
    <td><?php echo utf8_decode($row['nombres']); ?></td>
    <td><?php echo utf8_decode($row['primer_apellido']); ?></td>
    <td><?php echo utf8_decode($row['segundo_apellido']); ?></td>
    <td><?php echo $row['rut']?>-<?php echo $row['dv']; ?></td>
    <td><?php echo $row['numero_ficha']; ?></td>
    <td><?php echo $row['fecha']; ?></td>
    <td><?php echo utf8_decode($row['descripcion']); ?></td>
    <td><?php echo utf8_decode($row['presentacion']); ?></td>
    <td><?php echo $row['lateralidad']; ?></td>
    <td><?php echo $row['cantidad']; ?></td>
    <td><?php echo $row['tipo']; ?></td>
    <td><?php echo utf8_decode($row['observaciones']); ?></td>
<?php
}
?>

 </tr>
   </table>
 
 </div>
</body>
</html>
