
<?php
header("Content-type: application/vnd.ms-excel;charset=utf-8");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=Reporte.xls");

?>


<html>
<body>

<h1> Estados de Biopsias: </h1>
<br/>

<?php

$fecha1 = $_GET['fecha1'];
$fecha1=date("Y-m-d",strtotime($fecha1));

$fecha2 = $_GET['fecha2'];
$fecha2=date("Y-m-d",strtotime($fecha2));

include ('../config/conectar_bd.php');

//$query2 = "SELECT * FROM muestras WHERE fecha BETWEEN '$fecha1' AND '$fecha2'";

$Sqlquery = "select mu.numero_examen, pa.nombres, pa.primer_apellido, pa.segundo_apellido, pa.rut, pa.dv, pa.edad, org.descripcion, mu.fecha, inf.fecha_entrega_informe, inf.fecha_archiva_informe, ex.diagnosticos_clinicos
from pacientes as pa
	left join examenes as ex on pa.rut = ex.rut_paciente
		left join muestras as mu using (id_examen)
			left join informes as inf using (id_examen)
				left join cieo_organos as org on org.codigo = mu.cod_cieo
	where mu.fecha BETWEEN '$fecha1' and '$fecha2'
	order by fecha asc";

//echo $Sqlquery;

$query = pg_query($conexion, $Sqlquery);


//if (!$query ) { 
//echo "An error occurred.\n" ; 
//exit; 
//} 



?>

<table width="1000" border="1" align="center">
  <tr align="center" bgcolor="#FF9900">
    <td>N</td>
    <td>N biopsia</td>
    <td>Nombres</td>
    <td>Apellido Paterno</td>
    <td>Apellido Materno</td>
    <td>Rut</td>
    <td>Fecha Nacimiento</td>
    <td>Edad</td>
    <td>Organo</td>
    <td>Fecha Ingreso</td>
    <td>Fecha Informe</td>
    <td>Fecha despacho</td>
    <td>Diagnostico</td>
    
   
  </tr>

<?php

$cont=1;
while($row = pg_fetch_array($query,NULL,PGSQL_ASSOC))
{
?>

<tr align='center'>

    <td><?php echo $cont++; ?></td>
    <td><?php echo $row['numero_examen']; ?></td>
    <td><?php echo utf8_decode($row['nombres']); ?></td>
    <td><?php echo utf8_decode($row['primer_apellido']); ?></td>
    <td><?php echo utf8_decode($row['segundo_apellido']); ?></td>
    <td><?php echo $row['rut']?>-<?php echo $row['dv']; ?></td>
    <td><?php echo $row['fecha_nacimiento']; ?></td>
    <td><?php echo $row['edad']; ?></td>
    <td><?php echo utf8_decode($row['descripcion']); ?></td>
    <td><?php echo $row['fecha']; ?></td>
    <td><?php echo $row['fecha_entrega_informe']; ?></td>
    <td><?php echo $row['fecha_archiva_informe']; ?></td>
    <td><?php echo utf8_decode($row['diagnosticos_clinicos']); ?></td>
<?php
}
?>

 </tr>
   </table>

 </div>
</body>
</html>
