
<?php
header("Content-type: application/vnd.ms-excel;charset=utf-8");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=Reporte.xls");

?>


<html>
<body>

<h1> Informe: </h1>
<br/>
<?php

$fecha1 = $_GET['fecha1'];
$fecha1=date("Y-m-d",strtotime($fecha1));

$fecha2 = $_GET['fecha2'];
$fecha2=date("Y-m-d",strtotime($fecha2));

include ('../config/conectar_bd.php');

//$query2 = "SELECT * FROM muestras WHERE fecha BETWEEN '$fecha1' AND '$fecha2'";

$Sqlquery = "select  pac.nombres,pac.primer_apellido,pac.segundo_apellido,pac.rut,pac.dv,pac.numero_ficha,
presentacion,e.id_unidad,mu.fecha,e.rut_profesional_solicita,med.nombres as nombre_med,med.apellido_paterno,med.apellido_materno,
u.descripcion_unidad,mu.id_muestra,e.id_examen,es.descripcion_estado,fun.rut,fun.nombre,
mu.rut_recepcion,fecha_traslado,funtra.nombre as funcionario_traslado,funrec.nombre as funcionario_recepcion,mu.fecha_recepcion,
eest.id_estado,es.descripcion_estado,  
org.descripcion
from examenes e,unidades u,estados es,examen_estado eest,funcionarios fun,pacientes pac,muestras mu,
funcionarios funtra,funcionarios funrec,cieo_organos org,medicos med
where e.id_unidad=u.id_unidad
and eest.id_estado=es.id_estado
and e.id_examen=eest.id_examen
and e.rut_digita_aud=fun.rut
and e.rut_paciente=pac.rut
and mu.id_examen=e.id_examen
and mu.rut_recepcion=funtra.rut
and mu.rut_recepcion=funrec.rut
and mu.cod_cieo=org.codigo
and e.rut_profesional_solicita=med.rut

and mu.fecha BETWEEN '$fecha1' and '$fecha2'
order by u.descripcion_unidad,id_examen,
es.descripcion_estado";

	

//echo $Sqlquery;

$query = pg_query($conexion, $Sqlquery);


//if (!$query ) { 
//echo "An error occurred.\n" ; 
//exit; 
//} 



?>

<table width="1000" border="1" align="center">
  <tr align="center" bgcolor="#FF6600">
    <td colspan="9">ANTECEDENTES DEL CASO</td>
    <td colspan="4">1° TRASPASO </td>
    <td colspan="5">2° TRASPASO - ANATOM&Iacute;A PATOL&Oacute;GICA</td>
    <td colspan="10">PROCESAMIENTO</td>
    <td colspan="2">RESCATE INFORMES</td>
    
  </tr>
  
  <tr align="center" bgcolor="#FF9900">
    <td>No</td>
    <td>NOMBRE</td>
    <td>PRIMER APELLIDO</td>
    <td>SEGUNDO APELLIDO</td>
    <td>RUT</td>
    <td>FICHA</td>
    <td>TIPO DE MUESTRA</td>
    <td>N° ID MUESTRAS OBTENIDAS</td>
    <td>FECHA DE OBTENCI&Oacute;N</td>
    <td>NOMBRE  CIRUJANO</td>
    <td>UNIDAD</td>
    <td>NOMBRE ESTAFETA</td>
    <td>FECHA TRASLADO</td>
    <td>N° DE MUESTRAS TRASLADADAS</td>
    <td>NOMBRE RECEPCIONISTA</td>
    <td>FECHA RECEPCIÓN</td>
    <td>N° MUESTRAS RECIBIDAS</td>
    <td>N° MUESTRAS RECHAZADAS</td>
    <td>MOTIVO RECHAZO</td>
    <td>C&Oacute;DIGO MUESTRAS POR CASO</td>
    <td>NOMBRE TECN&Oacute;LOGO QUE PROCESA</td>
    <td>PATÓLOGO ASIGNADO</td>
    <td>N° MUESTRAS PROCESADAS</td>
    <td>INFORMES OBTENIDOS</td>
    <td>FECHA DEL INFORME</td>
    <td>FECHA DEL INFORME VALIDADO</td>
    <td>EVENTOS ADVERSOS (si / no)</td>
    <td>TIPO EVENTO ADVERSO</td>
    <td>CASOS CR&Iacute;TICOS (si / no)</td>
    <td>FECHA DE RESCATE EN SOFTWARE</td>
    <td>NOMBRE USUARIO QUE RESCATA</td>
    
    
   
  </tr>

<?php

$cont=1;
while($row = pg_fetch_array($query,NULL,PGSQL_ASSOC))
{
?>

<tr align='center'>

	<td><?php echo $cont++; ?></td>
    <td><?php echo utf8_decode($row['nombres']); ?></td>
    <td><?php echo utf8_decode($row['primer_apellido']); ?></td>
    <td><?php echo utf8_decode($row['segundo_apellido']); ?></td>
    <td><?php echo $row['rut']?>-<?php echo $row['dv']; ?></td>
    <td><?php echo $row['numero_ficha']; ?></td>
    
    
    <td><?php echo utf8_decode($row['presentacion']); ?></td>
    <td><?php echo $row['id_muestra']; ?></td>
    
    
    <td><?php echo $row['fecha']; ?></td>
    
    
    
    <td><?php echo utf8_decode($row['nombre_med']); ?> &nbsp; <?php echo utf8_decode($row['apellido_paterno']); ?> &nbsp; <?php echo utf8_decode($row['apellido_materno']); ?> </td>
    <td><?php echo utf8_decode($row['descripcion_unidad']); ?></td>
    <td><?php echo utf8_decode($row['funcionario_traslado']); ?></td>
    <td><?php echo $row['fecha_traslado']; ?></td>
    <td><?php echo $row['N° DE MUESTRAS TRASLADADAS']; ?></td>
    <td><?php echo utf8_decode($row['funcionario_recepcion']); ?></td>
    <td><?php echo $row['fecha_recepcion']; ?></td>
    <td><?php echo $row['N° MUESTRAS RECIBIDAS']; ?></td>
    <td><?php echo $row['N° MUESTRAS RECHAZADAS']; ?></td>
    <td><?php echo utf8_decode($row['MOTIVO RECHAZO']); ?></td>
    <td><?php echo utf8_decode($row['CÓDIGO MUESTRAS POR CASO']); ?></td>
    <td><?php echo utf8_decode($row['NOMBRE TECNÓLOGO QUE PROCESA']); ?></td>
    <td><?php echo utf8_decode($row['PATÓLOGO ASIGNADO']); ?></td>
    <td><?php echo $row['N° MUESTRAS PROCESADAS']; ?></td>
    <td><?php echo utf8_decode($row['INFORMES OBTENIDOS']); ?></td>
    <td><?php echo $row['FECHA DEL INFORME']; ?></td>
    <td><?php echo $row['FECHA DEL INFORME VALIDADO']; ?></td>
    <td><?php echo utf8_decode($row['EVENTOS ADVERSOS (si / no)']); ?></td>
    <td><?php echo utf8_decode($row['TIPO EVENTO ADVERSO']); ?></td>
    <td><?php echo utf8_decode($row['CASOS CRÍTICOS (si / no)']); ?></td>
    <td><?php echo $row['FECHA DE RESCATE EN SOFTWARE']; ?></td>
    <td><?php echo utf8_decode($row['NOMBRE USUARIO QUE RESCATA']); ?></td>
   
    
<?php
}
?>

 </tr>
   </table>

 </div>
</body>
</html>
