<?php
if(session_id()==''){session_start();}
include("../config/conectar_bd.php");
include("../config/funciones_f.php");
$xrut_login = $_SESSION['rut_login'];
$xperfil=$_SESSION['perfil'];
$cargo=$_SESSION['cargo'];

$zSql="SELECT examen_macroscopico, examen_microscopico, diagnostico_histologico FROM informes WHERE id_examen='$id_registro'";
$queryM=pg_Exec($conexion, $zSql);
$rowM=pg_fetch_array($queryM);

$usql="SELECT cargo FROM tra_usuario WHERE rut_usuario = $xrut_login";
$uquery=pg_Exec($conexion, $usql);
$urow=pg_fetch_array($uquery);
$cargo=$urow['cargo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style>
/* ESTILO TABLA */
.tabla1{
	width:600px;
}
.titulo{
	background-color:#636970;
	color:#FFF;
	text-align:center;
}
.titulo1{
	background-color:#636970;
	color:#FFF;
	text-align:center;
	border-radius:7px;
	overflow:hidden;
	-moz-box-shadow: 0px 0px 13px 6px #ccc;
    -webkit-box-shadow: 0px 0px 13px 6px #ccc;
    box-shadow: 0px 0px 10px 3px #ccc;
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111')";
    /* IE 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111');
	
}
.seleccionado:hover{
	background-color:#FFC;
	cursor:pointer;
}

.td_estilo_redondo
{
	border: 1px solid grey;
	margin:0;
	padding:0;
	border-radius:7px;
	overflow:hidden;
	-moz-box-shadow: 0px 0px 13px 6px #ccc;
    -webkit-box-shadow: 0px 0px 13px 6px #ccc;
    box-shadow: 0px 0px 10px 3px #ccc;
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111')";
    /* IE 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111');
}
.td_estilo_redondo1
{
	border-radius:7px;
	-moz-box-shadow: 0px 0px 13px 6px #ccc;
    -webkit-box-shadow: 0px 0px 13px 6px #ccc;
    box-shadow: 0px 0px 10px 3px #ccc;
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111')";
    /* IE 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111');
}
.input_text
{
	height:30px;
	border: 1px solid grey;
	margin:0;
	padding: 3px 0px 3px 10px;
	border-radius:7px;
	overflow:hidden;
	-moz-box-shadow: 0px 0px 10px 3px #ccc;
    -webkit-box-shadow: 0px 0px 10px 3px #ccc;
    box-shadow: 0px 0px 10px 3px #ccc;*/
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111')";
    /* IE 5.5 - 7 */
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111');
}

.input_text_mod
{
	border-top:hidden;
	border-left:none;
	border-right:none;
	border-bottom: groove;
}
/*input[type=text]
{
	height:20px;
	border: 1px solid grey;
	margin:0;
	padding: 3px 0px 3px 10px;
	border-radius:7px;
	overflow:hidden;
	-moz-box-shadow: 0px 0px 10px 3px #ccc;
    -webkit-box-shadow: 0px 0px 10px 3px #ccc;
    box-shadow: 0px 0px 10px 3px #ccc;*/
    /* IE 8 
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111')";*/
    /* IE 5.5 - 7 
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#111111');*/
/*}*/
input[type=text]:focus, textarea:focus {
  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 3px 0px 3px 10px;
  /*margin: 5px 1px 3px 0px;*/
  border: 1px solid rgba(81, 203, 238, 1);
}

.lineas {
border-bottom-color:#CCC;
border-bottom-style:dashed;
border-bottom-width:thin;
}
</style>
</head>
<body onload="chek();">

<div id='div_informe' >
<div id="div_mostrar_digitalizar">
	
</div>
<table width="900" border="0" class="td_estilo_redondo" style="margin-left:auto;margin-right:auto;">


<tr>
  <td width="420">
    <p>&nbsp;</p>
    <table width="900" border="0">
      <tr>
        <td width="515">&nbsp;</td>
        <td width="381"><table width="381" border="0">
          <tr>
            <td align="center"><input type="text" name="hora_recepcion" id="hora_recepcion" size="10" class="input_text"/></td>
            <td align="center"><input type="text" name="fecha_recepcion" id="fecha_recepcion" size="10" class="input_text"/></td>
            <td align="center"><input type="text" name="num_examen" id="num_examen" size="10" class="input_text"/></td>
            </tr>
          <tr>
            <td align="center" style="font-size:11px">HORA RECEPCIÓN</td>
            <td align="center" style="font-size:11px">FECHA RECEPCIÓ</td>
            <td align="center" style="font-size:11px">N° DE EXAMEN</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><p style="font-size:26px;font-weight:bold;margin-top:5px;margin-bottom:5px;">SOLICITUD DE BIOPSIA/CITOLOGÍA ANATOMIA PATOLOGICA</p></td>
        </tr>
      <tr>
        <td colspan="2"><p style="text-decoration:underline;margin-top:5px;font-weight:bold;margin-left:30px">IDENTIFICACION DEL PACIENTE</p>
          <table width="900" border="0">
            <tr>
              <td width="312" align="center"><p>
                <input type="text" name="nom_paciente" id="nom_paciente" size="40" class="input_text_mod"/>
              </p>
                <p style="font-size:12px">NOMBRES</p></td>
              <td width="249" align="center"><p>
                <input type="text" name="apep_paciente" id="apep_paciente" size="35" class="input_text_mod"/>
              </p>
                <p style="font-size:12px">APELLIDO PATERNO</p></td>
              <td width="325" align="center"><p>
                <input type="text" name="apem_paciente" id="apem_paciente" size="35" class="input_text_mod"/>
              </p>
                <p style="font-size:12px">APELLIDO MATERNO</p></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td width="145" style="font-size:12px;padding-left:30px"> RUT</td>
              <td width="313"><input type="text" name="rut" id="rut" size="30" class="input_text_mod"/></td>
              <td width="116" style="font-size:12px">FICHA</td>
              <td width="308"><input type="text" name="ficha" id="ficha" size="39" class="input_text_mod"/></td>
            </tr>
            <tr>
              <td style="font-size:12px;padding-left:30px">&nbsp;</td>
              <td width="313">&nbsp;</td>
              <td width="116" style="font-size:12px">&nbsp;</td>
              <td width="308">&nbsp;</td>
            </tr>
            <tr>
              <td width="145" style="font-size:12px;padding-left:30px">FECHA NACIMIENTO</td>
              <td width="313"><input type="text" name="fec_nac_paciente" id="fec_nac_paciente" size="30" class="input_text_mod"/></td>
              <td width="116" style="font-size:12px">TELEFONO</td>
              <td width="308"><input type="text" name="fono_paciente" id="fono_paciente" size="39" class="input_text_mod"/></td>
            </tr>
        </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td width="143" style="font-size:12px;padding-left:30px">SEXO</td>
              <td width="36"><p style="font-size:12px;">F</p></td>
              <td width="73"><input type="text" name="sexo_pac_f" id="sexo_pac_f" size="5" class="input_text_mod"/></td>
              <td width="35"><p style="font-size:12px;">M</p></td>
              <td width="162"><input type="text" name="sexo_pac_m" id="sexo_pac_m" size="5" class="input_text_mod"/></td>
              <td width="115" style="font-size:12px;">PREVISIÓN</td>
              <td width="153" style="font-size:10px;">BENEFICIARIO
                <input type="text" name="prevision_beneficiario" id="prevision_beneficiario" size="5" class="input_text_mod"/></td>
              <td width="149" style="font-size:10px;">PRIVADO
                <input type="text" name="prevision_privado" id="prevision_privado" size="5" class="input_text_mod"/></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td width="335" align="center"><p style="font-size:12px">
                <input type="text" name="procedencia_paciente" id="procedencia_paciente" size="40" class="input_text_mod"/>
                </p>
<p style="font-size:12px">PROCEDENCIA</p></td>
              <td width="200" align="center"><p style="font-size:12px">
                <input type="text" name="fecha_toma_muestra" id="fecha_toma_muestra" size="25" class="input_text_mod"/>
                </p>
                <p style="font-size:12px">FECHA TOMA DE MUESTRA</p></td>
              <td width="234" align="center"><p style="font-size:12px">
                <input type="text" name="hora_toma_muestra" id="hora_toma_muestra" size="25" class="input_text_mod"/>
              </p>
                <p style="font-size:12px">HORA TOMA DE MUESTRA </p></td>
              <td width="113" align="center"><p style="font-size:12px">
                <input type="text" name="biopsia_rapida" id="biopsia_rapida" size="5" class="input_text_mod"/>
                </p>
                <p style="font-size:12px">BIOPSIA RAPIDA </p></td>
              </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">MUESTRAS</p>
                <p>
                  <textarea name="textarea" id="textarea" cols="125" rows="5"></textarea>
                </p></td>
              </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">TIPO DE INTERVENCION
                </p>
                <table width="900" border="0">
                  <tr>
                    <td width="380"><input type="text" name="cod_cie" id="cod_cie" size="30" class="input_text_mod"/>
                      <select name="select" id="select">
                      <option value="0">SELECCIONAR</option>
                      </select></td>
                    <td width="510"><textarea name="sel_cod_cie" id="sel_cod_cie" cols="70" rows="2"></textarea></td>
                  </tr>
              </table></td>
            </tr>
        </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">DIAGNOSTICO CLINICO</p>
                <table width="900" border="0">
                  <tr>
                    <td><textarea name="diag_clinico" id="diag_clinico" cols="125" rows="5"></textarea></td>
                    </tr>
                </table></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">ANTECEDENTES</p>
                <table width="900" border="0">
                  <tr>
                    <td><textarea name="antecedentes" id="antecedentes" cols="125" rows="5"></textarea></td>
                  </tr>
                </table></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">INFORMACION ESPECIAL SOLICITADA</p>
                <table width="900" border="0">
                  <tr>
                    <td><textarea name="inf_especial" id="inf_especial" cols="125" rows="5"></textarea></td>
                  </tr>
                </table></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><p style="font-weight:bold ">ANTECEDENTES GINECOLOGICOS ( si procede )</p>
                <table width="900" border="0">
                  <tr>
                    <td width="239"><p style="font-size:12px">PAP ALTERADO
                      <select name="pap_alt" id="pap_alt">
                        <option value="0" selected="selected">SELECCIONAR</option>
                        <option value="1">SI</option>
                        <option value="2">NO</option>
                      </select>
                    </p></td>
                    <td width="356"><p style="font-size:12px">TRATAMIENTOS PREVIOS
                      <input type="text" name="tratamientos_prev" id="tratamientos_prev" size="30" class="input_text_mod"/>
                    </p></td>
                    <td width="291">CONO: </td>
                  </tr>
                </table></td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><table width="900" border="0">
                <tr>
                    <td width="155"><p style="font-size:12px"><span style="font-weight:bold ">MEDICO RESPONSABLE</span></p></td>
                    <td width="283"><p style="font-size:12px">
                      <input type="text" name="procedencia_paciente2" id="procedencia_paciente2" size="40" class="input_text_mod"/>
                    </p>
                      <p style="font-size:12px">NOMBRE</p></td>
                    <td width="448"><p style="font-size:12px">
                      <input type="text" name="procedencia_paciente3" id="procedencia_paciente3" size="40" class="input_text_mod"/>
                    </p>
                      <p style="font-size:12px">FIRMA</p></td>
                  </tr>
              </table>
                ................................................................................................................................................................................................................................</td>
            </tr>
          </table>
          <table width="900" border="0" style="margin-top:20px">
            <tr>
              <td align="left"><table width="900" border="0">
                <tr>
                  <td width="200"><p style="font-size:12px"><span style="font-weight:bold ">USO ANATOMIA PATOLOGICA </span></p></td>
                  <td width="238"><p style="font-size:12px">&nbsp;</p></td>
                  <td width="448"><p style="font-size:12px">&nbsp;</p></td>
                </tr>
              </table>
                ................................................................................................................................................................................................................................</td>
            </tr>
          </table>
          <p>&nbsp;</p>
          <p>&nbsp;</p></td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <div id="div_codigos_fonasa">
    </div>
    </td>
</tr>


<tr>
	<td colspan = "95">
		<div id = "div_digitalizar_archivo" style="margin-top:15px; display: none;"> 
	      <!--<form method="post" enctype="multipart/form-data">-->
	           <table border="0" cellpadding="0" cellspacing="0">
		           	<tr>
		           		<td  align="left">
		           			Seleccione archivo<br />
		           			<input type="file" id="id_subir_archivo" />
		           			<button onclick="subir_archivo_informe()">Subir</button>
		           		</td>
		           	</tr>
	           </table> 
	        <!--</form>-->
	        
   		</div>
   		
   		<div id = "subir_audio" style="margin-top:15px; display: none;"> 
	      <!--<form method="post" enctype="multipart/form-data">-->
	           <table border="0" cellpadding="0" cellspacing="0">
		           	<tr>
		           		<td  align="left">
		           			Seleccione archivo de audio<br />
		           			<input type="file" id="id_archivo_audio" />
		           			<button onclick="subir_archivo_audio()">Subir</button>
		           		</td>
		           	</tr>
	           </table> 
	        <!--</form>-->
	        
   		</div>
	</td>
</tr>

<tr>
  <td colspan="5" align="center">
   
    <button id="id_salir" style="width:70px;margin-left:5px;cursor:pointer" onclick="guardar_informe()">
        <img src="../imagenes/Symbol-Check.png" style="width:30px; height:30px;" /><br />
            Guardar
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="imprimir_informe()"> 
        <img src="../imagenes/icono_print.png" style="width:30px; height:30px;" /><br />
            Imprimir
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="salir_informe()">
        <img src="../imagenes/Symbol-Delete.png" style="width:30px; height:30px;" /><br />
            Salir
    </button>   
    </td>
</tr>

</table>
</div>

</body>
</html>
