<script type="text/javascript">

$(function() {
	$( "#tabs" ).tabs({
		beforeLoad: function( event, ui ) {
			ui.jqXHR.error(function() {
			ui.panel.html(
				"Couldn't load this tab. We'll try to fix this as soon as possible. " +
				"If this wouldn't be a demo." );
			});
		}
	});	
});


</script>
<div id="div_solicitud_inmuhistoquimica">
	<div id="tabs" style="width: 100%;">
		<ul>
			<li>
				<a href="ajax/solicitud_inmunohistoquimica.ajax.tabs.php">
					Inmunohistoquimicas 
				</a>
			</li>
			<li>
				<a href="ajax/solicitud_histoquimica.ajax.tabs.php">
					Histoquimicas
				</a>
			</li>
			<li><a href="ajax/solicitud_histologica.tabs.php">Histologicas</a></li>
		</ul>
	</div>
</div>  