<script type="text/javascript">
function guardar_codigos(){
var id_examen            		=  $("#id_registro").val();
var id_year             		=  $('#id_year').val();	
var id_registro_codigo_informe  =  $("#id_registro_codigo_informe").val();

var multiplicador_cod           =  $('#multiplicador_cod').val();	
if (multiplicador_cod==''){
	multiplicador_cod=1;
}

$.ajax({
			type	: "POST",
			url		: "ajax/codigos_informe.ajax.php",
			data	: "accion=Save"+
			          "&id_examen="+id_examen+
					  "&id_registro_codigo_informe="+id_registro_codigo_informe+
					  "&tipo_biopsia_cod="+$('#tipo_biopsia_cod').val()+
					  "&codigo_cie10_cod="+$('#codigo_cie10_cod').val()+
					  "&codigo_cieo_cod="+$('#codigo_cieo_cod').val()+					  
					  "&codigo_fonasa_cod="+$('#codigo_fonasa_cod').val()+
					  "&multiplicador_cod="+multiplicador_cod+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				var datos = datos.split('|');
				if (datos[1]=='Error:'){
					alert(datos);
					}
				$("#div_codigos_informe").dialog("close");
				actualiza_div_codigos_informes();
				$("#id_registro_codigo_informe").val('');

			}
	});
	
}


function eliminar_codigos(){
var id_examen            		=  $("#id_registro").val();
var id_year             		=  $('#id_year').val();	
var id_registro_codigo_informe  =  $("#id_registro_codigo_informe").val();
$.ajax({
			type	: "POST",
			url		: "ajax/codigos_informe.ajax.php",
			data	: "accion=Delete"+
					  "&id_registro_codigo_informe="+id_registro_codigo_informe+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				//alert(datos);
				$("#div_codigos_informe").dialog("close");
				actualiza_div_codigos_informes();
				$("#id_registro_codigo_informe").val('');				
			}
	});
	
}


function salir_codigos(){
$("#div_codigos_informe").dialog("close");	
}

function filtrar_codigo_diagnostico(){
	
}
		
		
</script>

<div id='div_codigos_informe' >
<table border='0' class="tabla_listado">
<tr>
    <td>
    Clase<br />
    <select id="tipo_biopsia_cod">
    <option value="0"></option>    
    <option value="1">Incisional</option>
    <option value="2">Punción con Aguja Fina</option>
    <option value="3">Exicional</option>
    <option value="4">Ampliada</option>
    </select>
    </td>
</tr>


<tr>
    <td>
     Organo<br />
	 <?php
        $cSql="SELECT  
                 * 
             FROM 
                cieo_organos
			 ORDER BY
	           descripcion;";
        $query=pg_Exec($conexion, $cSql);	
        $s='<select id="codigo_cieo_cod" onchange="filtrar_codigo_diagnostico()"> ';
        $s.='<option value=""></option>';	
        while ($row=pg_fetch_array($query)){
          $s.="<option value='".$row['codigo']."'>".$row['codigo'].' '.substr($row['descripcion'],0,100)."</option>";	 
        }	
        $s.='</select>';
        echo $s;
     ?>
    
    </td>
</tr>


<tr>
    <td>
     Código CIE10<br />
     	<div style="display: none;"></div>
		 <?php
	        $cSql="SELECT  
	                 * 
	             FROM 
	                cieo_diagnostico;";
	        $query=pg_Exec($conexion, $cSql);	
	        $s='<select id="codigo_cie10_cod"> ';
	        $s.='<option value=""></option>';	
	        while ($row=pg_fetch_array($query)){
	          $s.="<option value='".$row['codigo']."'>".$row['codigo'].' '.substr($row['descripcion'],0,100)."</option>";	 
	        }	
	        $s.='</select>';
	        echo $s;
	     ?>
    </td>
</tr>


<tr>
    <td>
     Código Fonasa<br />
	 <?php
        $cSql="SELECT  
                 * 
             FROM 
                fonasa
			 WHERE 
			   visualizar_macro='S'
			  ORDER BY 
			  codigo;";
        $query=pg_Exec($conexion, $cSql);	
        $s='<select id="codigo_fonasa_cod"> ';
        $s.='<option value=""></option>';	
		$codigo='';
        while ($row=pg_fetch_array($query)){
		  if ($codigo != $row['codigo']){	
		   $s.="<option value='".$row['codigo']."'>".$row['codigo'].' '.substr($row['descripcion'],0,100)."</option>";	 
		  }
		  else
		  {
           $s.="<option value='".$row['codigo']."'>".'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.substr($row['descripcion'],0,100)."</option>";	 
		  }
		  $codigo = $row['codigo'];
        }	
        $s.='</select>';
        echo $s;
     ?>
    
    </td>
</tr>

<tr>
    <td>
     Multiplicador<br />
     <input id="multiplicador_cod" type="text" size="6">
     <input id="id_registro_codigo_informe" type="text" style="display:none;" value=''>
     
    
    </td>
</tr>


<tr>
  <td colspan="8">
   
    <button id="id_salir" style="width:80px; margin-left:10px;" onclick="guardar_codigos()">
        <img src="imagenes/Symbol-Check.png" style="width:30px; height:30px;" /><br />
            Guardar
    </button>
    <button id="id_salir" style="width:80px; margin-left:10px;" onclick="eliminar_codigos()"> 
        <img src="imagenes/equis.png" style="width:30px; height:30px;" /><br />
            Eliminar
    </button>
    <button id="id_salir" style="width:80px; margin-left:10px;" onclick="salir_codigos()">
        <img src="imagenes/salir.png" style="width:30px; height:30px;" /><br />
            Salir
    </button>
   <input type="text" id="id_registro_informe"  value="" style="display:none"/>
   </td>
</tr>

</table>
</div>