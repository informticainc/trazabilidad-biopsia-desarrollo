<?php
if(session_id()==''){session_start();}
include("../config/conectar_bd.php");
include("../config/funciones_f.php");
$xrut_login = $_SESSION['rut_login'];
$xperfil=$_SESSION['perfil'];
$cargo=$_SESSION['cargo'];

$zSql="SELECT examen_macroscopico, examen_microscopico, diagnostico_histologico FROM informes WHERE id_examen='$id_registro'";
$queryM=pg_Exec($conexion, $zSql);
$rowM=pg_fetch_array($queryM);

$usql="SELECT cargo FROM tra_usuario WHERE rut_usuario = $xrut_login";
$uquery=pg_Exec($conexion, $usql);
$urow=pg_fetch_array($uquery);
$cargo=$urow['cargo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript">

function guardar_informe(){
var id_examen            =  $("#id_registro").val();
var id_year              =  $('#id_year').val();	
var id_registro_informe  =  $("#numero_registro_informe").val();
var resultado_critico  	 =  $("#resultado_critico").attr('checked');
var retenido_informe  	 =  $("#retenido_informe").val();
if (resultado_critico == 'checked' ){
   resultado_critico='S';
}
else
{
   resultado_critico='N';
}
if (id_registro_informe==''){
	alert("Indique el informe a guardar");
	return;
	}
	
//alert(id_registro_informe);	
$.ajax({
			type	: "POST",
			url		: "ajax/informe.ajax.php",
			data	: "accion=Save"+
			          "&id_examen="+id_examen+
					  "&id_registro_informe="+id_registro_informe+
					  "&numero="+$("#numero_informe_inf").val()+
					  "&examen_microscopico="+$("#examen_microscopico_inf").val()+
					  "&examen_macroscopico="+$("#examen_macroscopico_inf").val()+
                      "&diagnostico_histologico="+$("#diagnostico_histologico_inf").val()+					  
					  "&year="+id_year+
					  "&codigos_cieo="+
					  "&resultado_critico="+resultado_critico+
  				      "&retenido_informe="+retenido_informe+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				//alert(datos);
   				    $("#id_registro_informe_reconocimiento").val('');	
                     var datos =datos.split('|');
					 if (datos[1] == 'NO'){
						alert("Este informe se encuentra bloqueado, por tanto no es posible modificarlo"); 
					 }
					 else
					 {
					 
					 $('#id_examen_macroscopico_sol')	 .val($('#examen_macroscopico_inf')	 .val());
					 alert("Registro grabado");
					 //informe_microscopia('recepcion');
					 salir_informe();
					 }
			}
	});
	
}

function salir_informe(){
$("#id_registro_informe_reconocimiento").val('');	
$("#div_informe").dialog("close");	
if ($("#id_perfil").val()=='3'){
	
	buscar();
	}
}


function imprimir_informe(){
var id_registro_informe      =  $("#id_registro_informe").val();
var id_registro              =  $("#id_registro").val();
var numero_informe_inf       =  $("#numero_examen").val();
var examen_microscopico_inf  =  $("#examen_microscopico_inf").val();
var examen_macroscopico_inf  =  $("#examen_macroscopico_inf").val();
var responsable_informe       =  $("#responsable_informe").val();
var diagnostico_histologico_inf = $("#diagnostico_histologico_inf").val();
var clase_informe='';
if (id_registro_informe==''){
	alert("Debe guardar el informe antes de imprimir");
	return;
	}
	
$.ajax({
			type	: "POST",
			url		: "ajax/clase_informe.ajax.php",
			data	: "id_registro_informe="+id_registro_informe+
			          "&id_registro="+id_registro+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				
				datos = datos.trim();
				datos = datos.split('|');
				clase_informe=datos[1];
window.open("ajax/imprime_informe.ajax.php?id_registro="+id_registro+
				              "&numero_informe_inf="+numero_informe_inf+
				              "&examen_macroscopico_inf="+examen_macroscopico_inf+	
							  "&responsable_informe="+responsable_informe+						  
							  "&clase_informe="+clase_informe+	
							  "&diagnostico_histologico_inf="+diagnostico_histologico_inf+							  
							  "&examen_microscopico_inf="+examen_microscopico_inf,"mywindow","location=1,status=1,scrollbars=1, width=900,height=700");
			}
});
	
}

/*
function query(){
var xrut_login      =  $("#responsable_informe").val();
window.open("dialog/query.php?xrut_login="+xrut_login,"mywindow","location=1,status=1,scrollbars=1, width=900,height=700");
		
}*/


function refresh_informes(registro){
if (registro=='0'){	
//alert($('#numero_registro_informe').val());
	$("#id_registro_informe_reconocimiento").val($('#numero_registro_informe').val());
	var id_registro_informe =$('#numero_registro_informe').val();
}
else
{
	var id_registro_informe = registro;
	$("#id_registro_informe").val(id_registro_informe);
}

$.ajax({
			type	: "POST",
			url		: "ajax/revisar_audios_informe.ajax.php",
			data	: "id_registro_informe="+id_registro_informe+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				datos = datos.trim();
				datos = datos.split('|');
				if(datos[1] == 'vacio'){
					$('#div_audio_informe').html('');
				}else{
					$('#div_audio_informe').html(datos[1]);
				}
			}
});

$.ajax({
			type	: "POST",
			url		: "ajax/informe.ajax.php",
			data	: "accion=Refresh"+
					  "&id_registro_informe="+id_registro_informe+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
					  // alert(datos);
                      datos = datos.split('|');
				      $("#id_registro_informe").val(datos[2]);
                      $("#responsable_informe").val(datos[1]);
					 // $("#examen_macroscopico_inf").val($("#id_examen_macroscopico_sol").val());
					 // $("#examen_macroscopico_inf").val($("#id_examen_macroscopico_ppal").val());
					  if (datos[2]!=''){
						  $("#examen_macroscopico_inf").val(datos[7]);
					  }
					  else
					  {
						  $("#examen_macroscopico_inf").val('');
						  //$("#examen_macroscopico_inf").val($("#id_examen_macroscopico_sol").val());						  
					  }
					  if (datos[5] == '0'){
						  //$("#examen_macroscopico_inf").val($("#id_examen_macroscopico_sol").val());						  						  
			             // $("#examen_macroscopico_inf").attr('disabled', false);						  			              
					  }
					  else
					  {
						//$("#examen_macroscopico_inf").attr('disabled', true);										  
					  }
					  //var cargo_usu_t = $("#cargo_usu").val(datos[13]);
					  
					  $("#diagnostico_histologico_inf").val(datos[11]);
				      $("#examen_microscopico_inf").val(datos[4]);
					  $("#vob_informe").val(datos[5]);
					  $("#retenido_informe").val(datos[12]);
					  //$("#cargo_usu").val(datos[13]);
					  $("#nombre_archivo_informe").val(datos[10]+'/'+datos[6]);
					  verifica_ret();


if (datos[9] == 'S' ){
  $("#resultado_critico").attr('checked',true);

}
else
{
  $("#resultado_critico").attr('checked',false);
}

	/*alert("Ficha Actualizada");*/				  
			}
	});
	
}
	
	
function digitalizar_informe_firmado(){
 var examen_microscopico_inf = $("#examen_microscopico_inf").val();
 var vob_informe             = $("#vob_informe").val();
 
 if (vob_informe!='0'){
	  alert("Este informe se encuentra bloqueado");
	  $("#div_digitalizar_archivo").css('display','none');
	  return;
 }
 
  if (examen_microscopico_inf==''){
	  //alert("No existe un exámen microscópico para este informe");
	  //$("#div_digitalizar_archivo").css('display','none');
	  //return;
	  }
	  
	if( $("#div_digitalizar_archivo").css('display') == 'none') {
  		  $("#div_digitalizar_archivo").css('display','block');
	}else{
		$("#div_digitalizar_archivo").css('display','none');
	}
  
}	


function subir_archivo_informe(){
	
	var id_registro_informe = $('#id_registro_informe_reconocimiento').val();
	
	if(id_registro_informe == ''){
		var id_registro_informe = $('#id_registro_informe').val();	
	}


	var id_subir_archivo = $("#id_subir_archivo").val();
	
	if(id_registro_informe == ''){
		alert('Debe seleccionar algun informe para poder digitalizar');
		return;
	}
	
	if(id_subir_archivo == ''){
		alert('Debe seleccionar algun archivo para poder digitalizar');
		return;
	}

    var inputfile = document.getElementById("id_subir_archivo");
    var file = inputfile.files[0];
	var formdata = new FormData();
	formdata.append('archivo',file);
	

	$.ajax({
		url:"ajax/subir_informe_firmado.ajax.php?id_registro_informe="+id_registro_informe,
		type:'POST',
		data:formdata,
		dataType	: "html",
		contentType:false,
		processData:false,
		cache:false,
		beforeSend: function(){
			//$('#div_cargando_dialog').css('display','block');
		},
		success	: function(datos){
			//alert(datos);
			//$("#div_mostrar_digitalizar").html(datos);
			
			if(datos == 1){
				alert("Se ha subido correctamente el archivo");
				salir_informe();
			}else{
				alert("Ha ocurrido un error, comuníquese con informática ");
				salir_informe();
			}
			
			$("#id_subir_archivo").val('');
			$("#div_digitalizar_archivo").css('display','none');
		}
	});
}
		
function ver_digitalizado(){
var nombre_archivo_informe = $("#nombre_archivo_informe").val();	
if (nombre_archivo_informe =='' ){
	alert("No hay existe informe digitalizado");
	return;
}


abre_pdf(nombre_archivo_informe);
}		

function firma_digital(){
var archivo = "/home/web/biopsias/imagenes_digitalizadas/2014/"+$("#nombre_archivo_informe").val();	

$.ajax({
			type	: "POST",
			url		: "ajax/esigner.php",
			data	: "archivo="+archivo+
					  "&random="+Math.random(),
			dataType: "html",
			success	: function(datos){
				alert(datos);
			}
	});	
}
		
function subir_audio(){
	var vob_informe             = $("#vob_informe").val();
	 
	 if (vob_informe!='0'){
		  alert("Este informe se encuentra bloqueado");
		  $("#subir_audio").css('display','none');
		  return;
	}
	 
	if( $("#subir_audio").css('display') == 'none') {
  		  $("#subir_audio").css('display','block');
	}else{
		$("#subir_audio").css('display','none');
	}
 
}		
		
function subir_archivo_audio(){
	
	var id_archivo_audio 	= $('#id_archivo_audio').val();
	var id_registro_informe = $("#id_registro_informe").val();
	var rut_login 			= $("#id_rut_login").val();

	if(id_archivo_audio == ""){
		alert('Debe seleccionar alguns archivo de audio para subir');
		return;
	}

    var inputfile = document.getElementById("id_archivo_audio");
    var file = inputfile.files[0];
	var formdata = new FormData();
	formdata.append('archivo',file);
	
	$.ajax({
		url:"ajax/subir_archivo_audio.ajax.php?id_registro_informe="+id_registro_informe+"&rut_login="+rut_login,
		type:'POST',
		data:formdata,
		dataType	: "html",
		contentType:false,
		processData:false,
		cache:false,
		beforeSend: function(){
			var dialogo_cargando = $("#div_cargando_dialog").dialog(opt_dialogo_cargando);
			dialogo_cargando.dialog('open');	
		},
		success	: function(datos){
			
			$("#div_mostrar_digitalizar").html(datos);
			
			var dialogo_cargando = $("#div_cargando_dialog").dialog(opt_dialogo_cargando);
			dialogo_cargando.dialog('close');	
			
			if(datos == 1){
				alert("Se ha subido correctamente el archivo");
			}else{
				alert("Ha ocurrido un error, comuníquese con informática ");
			}
			
			$("#id_archivo_audio").val('');
			$("#subir_audio").css('display','none');
		}
	});
	
}		

function liberar_informe()
{
	document.getElementById('inf_ret').style.display = "none";
	document.getElementById('inf_ret1').style.display = "none";
	document.getElementById('inf_ret').checked = true;
	document.getElementById('retenido_informe').value = 0;
	document.getElementById('examen_macroscopico_inf').disabled = false;
	document.getElementById('examen_microscopico_inf').disabled = false;
	document.getElementById('diagnostico_histologico_inf').disabled = false;
	document.getElementById('boton_retener').disabled = false;
	document.getElementById('img_retener').disabled = false;
}

function check()
{
	
	document.getElementById('inf_ret').style.display = "";
	document.getElementById('inf_ret1').style.display = "";
	document.getElementById('inf_ret').checked = true;
	document.getElementById('retenido_informe').value = 1;
	document.getElementById('examen_macroscopico_inf').disabled = true;
	document.getElementById('examen_microscopico_inf').disabled = true;
	document.getElementById('diagnostico_histologico_inf').disabled = true;
	document.getElementById('boton_retener').disabled = true;
	document.getElementById('img_retener').disabled = true;
}

function verifica_ret()
{
	if(document.getElementById('cargo_usu').value == 'PATOLOGO')
	{

		if(document.getElementById('retenido_informe').value==1)
		{
			document.getElementById('examen_macroscopico_inf').disabled = true;
			document.getElementById('examen_microscopico_inf').disabled = true;
			document.getElementById('diagnostico_histologico_inf').disabled = true;
			document.getElementById('inf_ret').checked = true;
			document.getElementById('inf_ret').style.display='';
			document.getElementById('inf_ret1').style.display='';
			document.getElementById('inf_ret').disabled = false;
			document.getElementById('inf_ret1').disabled = false;
			document.getElementById('boton_retener').disabled = true;
			document.getElementById('img_retener').disabled = true;
		}
		else if(document.getElementById('retenido_informe').value==0)
		{
			document.getElementById('examen_macroscopico_inf').disabled = false;
			document.getElementById('examen_microscopico_inf').disabled = false;
			document.getElementById('diagnostico_histologico_inf').disabled = false;
			document.getElementById('inf_ret').checked = false;
			document.getElementById('boton_retener').disabled = false;
			document.getElementById('img_retener').disabled = false;
			document.getElementById('inf_ret').style.display='none';
			document.getElementById('inf_ret1').style.display='none';
		}
	}
	else if(document.getElementById('cargo_usu').value != 'PATOLOGO')
	{
		if(document.getElementById('retenido_informe').value==1)
		{
			document.getElementById('examen_macroscopico_inf').disabled = true;
			document.getElementById('examen_microscopico_inf').disabled = true;
			document.getElementById('diagnostico_histologico_inf').disabled = true;
			document.getElementById('inf_ret').checked = true;
			document.getElementById('boton_retener').disabled = true;
			document.getElementById('img_retener').disabled = true;
			document.getElementById('boton_retener').style.display='none';
			document.getElementById('img_retener').style.display='none';
			document.getElementById('inf_ret').style.display='none';
			document.getElementById('inf_ret1').style.display='none';
		}
		else if(document.getElementById('retenido_informe').value==0)
		{
			document.getElementById('examen_macroscopico_inf').disabled = false;
			document.getElementById('examen_microscopico_inf').disabled = false;
			document.getElementById('diagnostico_histologico_inf').disabled = false;
			document.getElementById('inf_ret').checked = false;
			document.getElementById('boton_retener').disabled = false;
			document.getElementById('img_retener').disabled = false;
			document.getElementById('boton_retener').style.display='';
			document.getElementById('img_retener').style.display='';
			document.getElementById('inf_ret').style.display='';
			document.getElementById('inf_ret1').style.display='';
			document.getElementById('inf_ret').disabled = false;
			document.getElementById('inf_ret1').disabled = false;
		}
	}
}

</script>

</head>
<body onload="chek();">

<div id='div_informe' >
<div id="div_mostrar_digitalizar">
	
</div>
<table border='0' class="tabla_listado">

<tr>
<td width="420" >
Informes
  <br />
<div id="div_informes_listado" style="float: left;">

</div>
<div id="div_audio_informe" style="float: left; margin-left: 10px;">
	
</div>
</td>
<td>

</td>
</tr>

<?php
//echo $_SESSION['cargo'];
//echo $urow['cargo'];
?>
<tr>
<td colspan="2">
Ex&aacute;men Macrosc&oacute;pico<br />
<textarea id="examen_macroscopico_inf" rows="4" cols="80" style="padding-left:7px;"></textarea>
</td>
</tr>


<tr>
	<td colspan="2">
		Ex&aacute;men Microsc&oacute;pico<br />
		<textarea id="examen_microscopico_inf" rows="5" cols="80" style="padding-left:7px;"></textarea>
	</td>
</tr>

<tr>
	<td colspan="2">
		Diagn&oacute;stico Histol&oacute;gico<br />
		<textarea id="diagnostico_histologico_inf" rows="5" cols="80" style="padding-left:7px;"></textarea>
	</td>
</tr>

<tr>
	<td colspan="2">
        <input id="resultado_critico" type="checkbox" style="padding-left:7px;"/>Resultado Cr&iacute;tico	    
        
        <input name="inf_ret" id="inf_ret" type="checkbox" style="padding-left:7px;display:none" onclick="liberar_informe();"/><span id="inf_ret1" style="display:none">Retener Informe</span>
        <input type="hidden" id="id_registro_informe" size="3"/>
        <input type="hidden" id="responsable_informe" size="3"/>
        <input type="hidden" id="id_registro_informe_reconocimiento" size="3"/>
        <input type="hidden" id="vob_informe" size="3"/>
        <input type="hidden" id="retenido_informe" size="3"/>
        <input type="hidden" id="nombre_archivo_informe" size="3"/>
        <input type="hidden" id="cargo_usu" size="6" value="<?php echo $cargo;?>"/></td>
</tr>


<tr>
	<td colspan="6">
     <div id="div_codigos_fonasa">
     
     </div>
	</td>
</tr>


<tr>
	<td colspan = "100%">
		<div id = "div_digitalizar_archivo" style="margin-top:15px; display: none;"> 
	      <!--<form method="post" enctype="multipart/form-data">-->
	           <table border="0" cellpadding="0" cellspacing="0">
		           	<tr>
		           		<td  align="left">
		           			Seleccione archivo<br />
		           			<input type="file" id="id_subir_archivo" />
		           			<button onclick="subir_archivo_informe()">Subir</button>
		           		</td>
		           	</tr>
	           </table> 
	        <!--</form>-->
	        
   		</div>
   		
   		<div id = "subir_audio" style="margin-top:15px; display: none;"> 
	      <!--<form method="post" enctype="multipart/form-data">-->
	           <table border="0" cellpadding="0" cellspacing="0">
		           	<tr>
		           		<td  align="left">
		           			Seleccione archivo de audio<br />
		           			<input type="file" id="id_archivo_audio" />
		           			<button onclick="subir_archivo_audio()">Subir</button>
		           		</td>
		           	</tr>
	           </table> 
	        <!--</form>-->
	        
   		</div>
	</td>
</tr>

<tr>
  <td colspan="10">
   
    <button id="id_salir" style="width:70px;margin-left:5px;cursor:pointer" onclick="guardar_informe()">
        <img src="imagenes/Symbol-Check.png" style="width:30px; height:30px;" /><br />
            Guardar
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="imprimir_informe()"> 
        <img src="imagenes/icono_print.png" style="width:30px; height:30px;" /><br />
            Imprimir
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="digitalizar_informe_firmado()">
        <img src="imagenes/scanner.png" style="width:30px; height:30px;" /><br />
            Digitalizar
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="subir_audio()">
        <img src="imagenes/audio.jpeg" style="width:30px; height:30px;" /><br />
             Audio
    </button>
    <button id="id_salir" style="width:70px; display:none;margin-left:5px;cursor:pointer" onclick="firma_digital()" >
        <img src="imagenes/esign.png" style="width:40px; height:30px;" /><br />
            Firmar
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="ver_digitalizado()">
        <img src="imagenes/macroscopia.png" style="width:30px; height:30px;" /><br />
            Ver
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="vob()"> 
        <img src="imagenes/Lock.png" style="width:30px; height:30px;" /><br />
           Bloquear
    </button>
    <button id="boton_retener" style="width:70px; margin-left:5px;cursor:pointer" onclick="vob1();"> 
        <img id="img_retener" src="imagenes/informe_critico.png" style="width:30px;height:30px;" /><br />
    	   Retener
    </button>
    <button id="id_salir" style="width:70px; margin-left:5px;cursor:pointer" onclick="salir_informe()">
        <img src="imagenes/salir.png" style="width:30px; height:30px;" /><br />
            Salir
    </button>   
    </td>
</tr>

</table>
</div>

</body>
</html>
