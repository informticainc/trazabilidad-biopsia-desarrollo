<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php



// Fecha: ene-11-2012
// Descripción: esta función retorna una cadena de texto a partir de una tabla, con las opciones para
// generar una lista desplegable.
// Parametros:
// $sSql  "Instrucciones sql"
// $cValue llave principal de la tabla (la que se asignara a la variable, esta no se muestra al Usuario)
// $cLlave1 Primer campo de la tabla que desea visualizar
// $cLlave2 Segundo campo de la tabla que desea visualizar

//Para Mysql

Function lista_desplegable ($cSql,$cValue,$cLlave1,$cLlave2,$ccondicion){
$result = mysql_query($cSql);
$opciones="";
global $cLlave;

//alert($ccondicion);
while ($row = mysql_fetch_array($result)) {
if (isset($cLlave2) && $cLlave2!="") {
   $cResto=$row[$cLlave1]." ".$row[$cLlave2]."</option>\n";}
else {
   $cResto=$row[$cLlave1]."</option>\n"; }
//		alert($cLlave." -> ".$row[$cValue]." ".gettype($cLlave));   
     if ($cLlave == $row[$cValue] ){
        $opciones=$opciones. "<option value=".$row[$cLlave1]." selected>".$cResto; 
      } else {
         $opciones=$opciones. "<option value=".$row[$cValue]." >".$cResto; 
    }
}
return $opciones;
}

//Para postgres
Function lista_desplegablepg ($cSql,$cValue,$cLlave1,$cLlave2,$ccondicion){

//$result = mysql_query($cSql);
$opciones="";
global $cLlave;
global $conexion;
$query = pg_Exec($conexion, $cSql);
while ($row = pg_fetch_array($query)) {
 if (isset($cLlave2) && $cLlave2!="") {
    $cResto=$row[$cLlave1]." ".$row[$cLlave2]."</option>\n";}
 else {
   $cResto=$row[$cLlave1]."</option>\n"; }
     if (rtrim($cLlave) == rtrim($row[$cValue]) ){
        $opciones=$opciones. "<option value=".$row[$cValue]." selected>".$cResto; 
      } else {
         $opciones=$opciones. "<option value=".$row[$cValue]." >".$cResto; 
 }
}
return $opciones;
}



//Retorna el valor de un campo especifico

//Mysql
Function traercampo ($cSql){
$result = mysql_query($cSql);
$respuesta="";
global $cLlave;
while ($row = mysql_fetch_array($result)) {
   $respuesta= $row[0]; 
}
return $respuesta;
}

//Postgres
Function pg_traercampo ($cSqlp,$conexion){
	
$query = pg_Exec($conexion, $cSqlp);
$respuesta="";

//if (!isset($ncampos)){
 //$ncampos=1;
//}

while ($row = pg_fetch_array($query)) {
   $respuesta= $row[0];
  // alert(sizeof($row));
  // if (sizeof($row)>1){
  //   $respuesta= $row[0].$row[1]; 	   
  // }
   
}
return $respuesta;
}


Function msg_href($sarta,$href){
	
 echo "<script language='JavaScript'>
 alert('".$sarta."');".$href."</script>";	
}

Function alert($sarta){
	
 echo "<script language='JavaScript'>
 alert('".$sarta."');
 </script>";	
}



function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) 
  {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case  "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}	

function haceselected($cvble,$coption){

 if (rtrim($cvble) == rtrim($coption))	{
  return " selected ";	
 }
 else{
  return "";	 
 }
}

function cambiarFormatoFecha($fecha){
	if ($fecha){ 
    list($anio,$mes,$dia)=explode("-",$fecha);
	$fe=$dia."/".$mes."/".$anio;
	$fe=str_replace('//','',$fe);
    return $fe;
	}
	return "";
} 

function cambiarFormatoFechaHora($fecha){
	$hora=substr($fecha,11,8);
	$fecha=substr($fecha,0,10);
	
	if ($fecha){ 
    list($anio,$mes,$dia)=explode("-",$fecha);
	$fe=$dia."/".$mes."/".$anio;
	$fe=str_replace('//','',$fe);
    return $fe.'   '.$hora;
	}
	return "";
} 


function resta_fechas($fecha1,$fecha2)
	{
		  if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
				  list($dia1,$mes1,$año1)=split("/",$fecha1);
		  if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
				  list($dia1,$mes1,$año1)=split("-",$fecha1);
			if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
				  list($dia2,$mes2,$año2)=split("/",$fecha2);
		  if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
				  list($dia2,$mes2,$año2)=split("-",$fecha2);
			$dif = mktime(0,0,0,$mes1,$dia1,$año1) - mktime(0,0,0,$mes2,$dia2,$año2);
		  $ndias=floor($dif/(24*60*60));
		  return($ndias);
	}
	
	

function dv($r){$s=1;for($m=0;$r!=0;$r/=10)$s=($s+$r%10*(9-$m++%6))%11;
$dv=chr($s?$s+47:75);
$dv=rtrim($dv);
return $dv;
}


function fecha_texto($fecha){

$dia=substr($fecha,0,2);
$mes=substr($fecha,3,2);
$year=substr($fecha,6,4);	
$s=$dia." de ";

switch ($mes) {
    case '01':
        $s.="Enero";
        break;
    case '02':
        $s.="Febrero";
        break;
    case '03':
        $s.="Marzo";
        break;
    case '04':
        $s.="Abril";
        break;
    case '05':
        $s.="Mayo";
        break;
    case '06':
        $s.="Junio";
        break;
    case '07':
        $s.="Julio";
        break;
    case '08':
        $s.="Agosto";
        break;
    case '09':
        $s.="Septiembre";
        break;
    case '10':
        $s.="Octubre";
        break;
    case '11':
        $s.="Noviembre";
        break;
    case '12':
        $s.="Diciembre";
        break;
}
$s.=' de '.$year;
return $s;




}

function get_fecha($fecha){

$fecha=substr($fecha,0,10);	
$fecha=cambiarFormatoFecha($fecha);
return $fecha;
}
	
function get_hora($fecha){
$hora = substr($fecha, 11, 5 );	
return $hora;
	}


function conversor_segundos($seg_ini) {

$horas = floor($seg_ini/3600);
$minutos = floor(($seg_ini-($horas*3600))/60);
$segundos = $seg_ini-($horas*3600)-($minutos*60);
//return  $horas.'h:'.$minutos.'m:'.$segundos.'s';
	
return  $horas.'hrs  : '.$minutos.'\' : '.$segundos.'"';

}

function conversor_tiempo($seg_ini) {

$horas = floor($seg_ini/3600);
$minutos = floor(($seg_ini-($horas*3600))/60);
$segundos = $seg_ini-($horas*3600)-($minutos*60);
//return  $horas.'h:'.$minutos.'m:'.$segundos.'s';
$dias='';
if ($horas>'24'){
    $dias=floor(($horas/24)).' Días<br/>';
	$horas=($horas % 24);

}
	
return  $dias.' '.$horas.' hrs : '.$minutos.'\' : '.$segundos.'"';

}

function dias_plazo($seg_ini) {

$horas = floor($seg_ini/3600);
$minutos = floor(($seg_ini-($horas*3600))/60);
$segundos = $seg_ini-($horas*3600)-($minutos*60);
//return  $horas.'h:'.$minutos.'m:'.$segundos.'s';
$dias='0';
if ($horas>'24'){
    $dias=floor(($horas/24));
}
	
return  $dias;

}


function estado_cassette($estado_cassette){
$icono = '<img src="imagenes/yellow.png" style="width:15px; height:15px;" />&nbsp;&nbsp;';
if ($estado_cassette == '1') return $icono.'Fijando';	
if ($estado_cassette == '2') return $icono.'Descalcificando';	
if ($estado_cassette == '3') return $icono.'Otra';	
if ($estado_cassette == '9') {
 $icono = '<img src="imagenes/green.png" style="width:15px; height:15px;" />&nbsp;&nbsp;';
 return $icono.'Finalizado';	
}
}


function estado_cassette_automatico($estado_cassette,$rut_automatico,$rut_inclusion,$rut_corte,$etapa){
$icono = '<td class="valor_listado"><img src="imagenes/red.png" style="width:15px; height:15px;" />&nbsp;&nbsp;';

if ($estado_cassette == '1') return $icono.'Fijando</td><td  class="valor_listado" bgcolor="#FF9900" width="15"></td>';	
if ($estado_cassette == '2') return $icono.'Descalcificando</td><td  class="valor_listado" bgcolor="#FF9900" width="15"></td>';	
if ($estado_cassette == '3') return $icono.'Otra</td><td  class="valor_listado" bgcolor="#FF9900" width="15"></td>';	


if ($estado_cassette == '9' && $etapa == 'Automático') {
 $icono = '<td  class="valor_listado"><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#F14E16" width="15"></td>';	
}

if ($estado_cassette == '9' && $etapa == 'Inclusión') {
 $icono = '<td  class="valor_listado"><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#3A5AF3" width="15"></td>';	
}

if ($estado_cassette == '9' && $etapa == 'Corte') {
 $icono = '<td  class="valor_listado"><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#DF01A5" width="15"></td>';	
}

if ($estado_cassette == '9' && $etapa == 'Entrega') {
 $icono = '<td  class="valor_listado"><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#38C609" width="15"></td>';	
}


if ($estado_cassette == '9' && $etapa == 'Archivo') {
 $icono = '<td  class="valor_listado"><img src="imagenes/green.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Finalizado</td><td bgcolor="#38C609" width="15"></td>';	

}

/*
if ($estado_cassette == '9' && $rut_automatico == '') {
 $icono = '<td><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#F14E16" width="15"></td>';	
}

if ($estado_cassette == '9' && $rut_inclusion == '') {
 $icono = '<td><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#3A5AF3" width="15"></td>';	
}

if ($estado_cassette == '9' && $rut_corte != '') {
 $icono = '<td><img src="imagenes/green.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Finalizado</td><td bgcolor="#DF01A5" width="15"></td>';	
}


if ($estado_cassette == '9' && $rut_corte == '') {
 $icono = '<td><img src="imagenes/yellow.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Pendiente</td><td bgcolor="#38C609" width="15"></td>';	
}

*/
/*
if ($estado_cassette == '9' && $rut_corte != '') {
 $icono = '<td><img src="imagenes/green.png" style="width:15px; height:15px;"  />&nbsp;&nbsp;';
 return $icono.'Finalizado</td><td bgcolor="#38C609" width="15"></td>';	
}
*/
}

function presentacion($presentacion){
if ($presentacion=='F'){ return 'Frasco';}	
if ($presentacion=='T'){ return 'Tubo';}	
if ($presentacion=='P'){ return 'Portaobjeto';}	
if ($presentacion=='A'){ return 'Taco';}
if ($presentacion=='L'){ return 'Lámina';}	
}

function tipo_muestra($tipo_muestra){
if ($tipo_muestra=='D'){ return 'Diferida';}	
if ($tipo_muestra=='R'){ return 'Rápida';}	
if ($tipo_muestra=='C'){ return 'Citología';}	
}
  
function espacios($cuantos){
	for ($i = 1; $i <= $cuantos; $i++) {
		echo "&nbsp;";
	} 	
}

?>


